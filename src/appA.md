# Complex function theory survival guide {#Chap:Compl}

Not every student may have had enough complex analysis to appreciate the
manipulations we must carry out when -- for example -- calculating
Green's functions in this course; others may have forgotten most of what
they've learnt about this subject. Hence, I list the most important
results of complex function theory without proof.

An *analytic* function is a complex function which can be differentiated
an infinite number of times. It turns out that a complex function which
is differentiable, satisfies the *Cauchy-Riemann* equations. In order to
formulate these equations, we first introduce some notation. A point in
the complex plane is given as $$z = x + {\rm i}y.$$ A complex function
$f(z)$ may then be written as $$f(z) = u(x,y) + {\rm i}v(x,y).$$ $u$ is
the real, and $v$ the imaginary part of the complex function. The
Cauchy-Riemann equations are then
$$\frac{\partial u}{\partial x} = \frac{\partial v}{\partial y} ; \phantom{xxx}
\frac{\partial u}{\partial y} = -\frac{\partial v}{\partial x}.$$ It
turns out that this condition is sufficiently strong to ensure infinite
differentiability: a complex function which can be differentiated once,
can be differentiated an infinite number of times. Functions that
satisfy this requirement are called *analytic*.

We often deal with integrations over closed curves ('contours'). It can
be shown that the integral of an analytic function taken over such a
contour gives zero: $$\oint_\Gamma f(z) dz = 0,$$ where $\Gamma$ denotes
the contour. We adopt the convention that in complex integration, the
contour is always traversed in the anti-clockwise direction. Reversing
the direction reverses the sign of the result (which in the case of an
analytic function has no effect, as the result of the integration is 0).

We often deal with functions having singularities. Point-like
singularities are called *poles*. We say that a function $f$ has a pole
of order $n$ in the point $a$ on the complex plane if $(z-a)^n f(z)$ is
analytic in $a$, but $(z-a)^{n-1} f(z)$ is not. Now suppose we expand
$f$ around $a$ as a series expansion in $z-a$, including negative
powers: $$f(z) = \sum_{n=-\infty}^\infty (z-a)^n f_n(a) .$$ The
*residue* of $f$ in $a$, denoted as ${\rm res}_{a} f$ is defined as the
coefficient of $(z-a)^{-1}$ in this expansion. For a pole of order one,
also a called a simple pole, the residue of $f$ is given as
$\lim_{z\rightarrow a} (z-a)f(a)$. In general, for an isolated pole, the
residue is defined as
$${\text{res}}_a f(z) = \frac{1}{(n-1)!} \lim_{z\rightarrow a} \frac{d^{n-1}}{dz^{n-1}} \left[ (z-a)^n f(z)\right].$$

The most important result of complex analysis that we shall be using
frequently, is about functions with a set of isolated poles
$a_1, a_2, \ldots, a_k$ *within* the closed contour $\Gamma$. We then
have
$$\oint_\Gamma f(z) dz = 2 \pi {\rm i}\sum_{j=1}^k {\rm res}_{a_j} f.$$
This is the so-called *residue theorem*.

Let us consider an example. We calculate the integral
$$\oint \frac{1}{1+z^2} dz$$ over a circle of radius 2 around the
origin. The contour contains the points $\pm {\rm i}$. We note that
$$\frac{1}{1+z^2} =\frac{{\rm i}}{2} \frac{1}{z+{\rm i}} - \frac{{\rm i}}{2} \frac{1}{z-{\rm i}} .$$
Both terms yield a standard integral with a simple pole, and working
them out using the residue theorem yields the value
$$\oint \frac{1}{1+z^2} dz = \frac{{\rm i}}{2} (2\pi {\rm i}) - \frac{{\rm i}}{2} (2\pi {\rm i}) = 0.$$

Another result is important for the cases we will be dealing with.
Consider a semi-circle with radius $R$ in the upper complex plane, and
centred around 0. We call this circle $\Gamma^+$. Then, *Jordan's lemma*
says that if the function $f(z)$ is bounded on $\Gamma^+$, the integral
$$\int_{\Gamma^+} e^{{\rm i}kz} f(z) dz$$ is finite, and for
$R\rightarrow \infty$ it approaches zero.

As an example, we calculate
$$\int_{-\infty}^\infty \frac{e^{{\rm i}kx}}{x - a} dx,$$ where $a$ is a
complex number with a positive imaginary part and $k$ is real. We now
evaluate the integral over the contour $\Gamma$ shown in
figure [A.1](#Fig:Contour). Because of Jordan's Lemma, this integral is
equal to the integral over the real axis only. Using the residue
theorem, we immediately have
$$\int_{-\infty}^\infty \frac{e^{{\rm i}kx}}{x - a} dx = \oint \frac{e^{{\rm i}kx}}{x - a} dx  - 
\int_{\Gamma^+} \frac{e^{{\rm i}kx}}{x - a} dx.$$ Now we use Jordan's
lemma which says that the second term vanishes, and the integral over
the closed contour can be evaluated using the residue theorem:
$$\oint \frac{e^{{\rm i}kx}}{x - a} dx = 2\pi {\rm i}e^{ika}.$$

<figure markdown>
![contour](figures/contour.svg){: width="700px" #Fig:Contour}
  <figcaption>FIGURE A.1: The contour for evaluating an integral along the real axis.
</figcaption>
</figure>

Finally, we consider the integral $$\int \frac{f(x)}{x} dx$$ over the
real axis. This integral is not well defined at $x=0$ unless $f$
vanishes there (and is continuously differentiable at $x=0$). Now let us
consider the same integral but running just above the real axis:
$$\int \frac{f(x+{\rm i}\epsilon)}{x+{\rm i}\epsilon} dx,$$ where
$\epsilon$ is small. Supposing again that $f$ is regular, the small
$\epsilon$ does not change its value significantly, but we have avoided
the singularity at $x=0$. For $x$ not very close to zero, the integral
is approximately equal to the one running over the real axis, and we
should focus on what happens near $x=0$. Let us work out the imaginary
part of $1/(x+{\rm i}\epsilon)$:
$${\rm Im} \left( \frac{1}{x+{\rm i}\epsilon}\right) = \frac{1}{2{\rm i}} \left( \frac{1}{x+{\rm i}\epsilon} - \frac{1}{x-{\rm i}\epsilon} \right) = -\frac{\epsilon}{x^2 + \epsilon^2}.$$
The right hand side of this function is a narrow peak centred around
$x=0$. Its integral is $-\pi$. Therefore, for small $\epsilon$, the
integral will be $-\pi \delta (x)$. All in all, we see that we can write
$$\frac{1}{x+{\rm i}\epsilon} = 1/x {\text{ (for }} x {\text{ away from zero) }} - {\rm i}\pi \delta(x).$$
The first part of this expression is called the *principal value* and
denoted as ${\mathcal P}$:
$$\frac{1}{x+{\rm i}\epsilon} = {\mathcal P} \left(\frac{1}{x}\right) - {\rm i}\pi \delta(x).$$
More precisely
$${\mathcal P} \left(\frac{1}{x}\right) = 1/x {\text{ for }} |x|>\epsilon, \epsilon \rightarrow 0.$$
For completeness, we write down the principal value integral over a
function $f(x)$ having a singularity in $a$ is:
$${\mathcal P} \int f(x) dx = \lim_{\epsilon\downarrow 0} 
 \int_{-\infty}^{a-\epsilon} f(x) dx + \int_{a+\epsilon}^\infty f(x) dx.$$
