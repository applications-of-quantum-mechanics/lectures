function initThebelab() {
    let activateButton = document.getElementById("thebelab-activate-button");
    
    document.getElementById("thebe-panel").style.display = "block";
    
    var def = document.querySelector('[data-md-color-scheme=default]');
    def.style.setProperty('--md-primary-fg-color', '#faaf00');
    
    var slate = document.querySelector('[data-md-color-scheme=slate][data-md-color-primary=indigo]');
    slate.style.setProperty('--md-primary-fg-color', '#faaf00');
    

    if (activateButton.classList.contains('thebelab-active')) {
        return;
    }
    thebelab.bootstrap();
    // Run all thebelab cells marked as initialization code
    setTimeout(function() {
        let initDivs = document.getElementsByClassName("thebelab-init-code");
        for(var i = 0; i < initDivs.length; i++) {
            // div.thebelab-init-code > div.thebelab-cell > button.thebelab-run-button
            let runButton = initDivs[i].firstElementChild.childNodes[1]
            runButton.click();
        }
    }, 500)
    activateButton.classList.add('thebelab-active');
    activateButton.style.display = "none"; 
}