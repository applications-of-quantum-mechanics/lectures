# 2.3. The path integral: from classical to quantum mechanics {#Sec:PathInt1}

The path integral is a very powerful concept for connecting classical
and quantum mechanics. Moreover, this formulation renders the connection
between quantum mechanics and statistical mechanics very explicit. We
shall restrict ourselves here to a discussion of the path integral in
quantum mechanics. The reader is advised to consult the excellent book
by Feynman and Hibbs (*Quantum Mechanics and Path Integrals*,
McGraw-Hill, 1965) for more details.

The path integral formulation can be derived from the following
heuristics, based on the analogy between particles and waves:

-   A point particle which moves with momentum ${\bf p}$ at energy $E$
    can also be viewed as a wave with a phase $\varphi$ given by
    $$\varphi = {\bf k}\cdot {\bf r}- \omega t$$ where
    ${\bf p}= \hslash {\bf k}$ and $E = \hslash \omega$.

-   For a single path, these phases are additive, i.e., to find the
    phase of the entire path, the phases for different segments of the
    path should be added.

-   The probablity to find a particle which at $t=t_0$ was at
    ${\bf r}_0$, at position ${\bf r}_1$ at time $t=t_1$, is given by
    the absolute square of the sum of the phase factors
    $\exp({\rm i}\varphi)$ of all possible paths leading from
    $({\bf r}_0, t_0)$ to $({\bf r}_1, t_1)$:
    $$P({\bf r}_0, t_0; {\bf r}_1, t_1) = \left| \sum_{\text{all paths}} e^{{\rm i}\varphi_{\text{path}}} 
    \right|^2.$$ This probability is defined up to a constant which can
    be fixed by normalisation (i.e. the term within the absolute bars
    must reduce to a delta-function in ${\bf r}_1-{\bf r}_0$).

These heuristics are the analog of the Huygens principle in wave optics.

To analyse the consequences of these heuristics, we chop the time
interval between $t_0$ and $t_1$ into many identical time slices (see
Fig. [2.3](#Fig:TimeSlic)) and consider one such slice. Within this slice we take the path to be
linear. 

<figure markdown>
![Time slice](figures/path.svg){: width="700px" #Fig:TimeSlic}
  <figcaption> A possible path running from an initial position $x_\text{i}$ at time $t_\text{i}$
to a final position $x_\text{f}$ at time $t_\text{f}$. The time is divided
up into many identical slices. </figcaption>
</figure>

To simplify the analysis we consider one-dimensional motion. We
first consider the contribution of $k x$ to the phase difference. If the
particle moves in a time $\Delta t$ over a distance $\Delta x$, we know
that its $k$-vector is given by
$$k = \frac{mv}{\hslash} = \frac{m \Delta x}{\hslash \Delta t}.$$ The
phase change resulting from the displacement of the particle can
therefore be given as
$$\Delta \varphi = k \Delta x = \frac{m \Delta x^2}{\hslash \Delta t}.$$
We still must add the contribution of $\omega \Delta t$ to the phase.
The frequency $\omega$ is related to the energy; we have
$$\hslash \omega = \frac{p^2}{2m} + V(x) = \frac{\hslash^2 k^2}{2m} + V(x).$$
Neglecting the potential energy we obtain
$$\Delta \varphi = \frac{m \Delta x^2}{\hslash \Delta t} - \frac{\hslash^2 k^2}{2m\hslash} \Delta t = 
\frac{m \Delta x^2}{2\hslash \Delta t}.$$ The potential also enters
through the $\omega \Delta t$ term, to give the result:
$$\Delta \varphi = \frac{m \Delta x^2}{2\hslash \Delta t} - \frac{V(x)}{\hslash}\Delta t.$$
For $x$ occurring in the potential we may choose any value between the
begin and end point -- the most accurate result is obtained by
substituting the average of the value at the beginning and at the end of
the time interval.

If we now use the fact that phases are additive, we see that for the
entire path the phases are given by
$$\varphi = \frac{1}{\hslash} \sum_j \left\{ \frac{m}{2} \left[ \frac{x(t_{j+1}) -
x(t_j)}{\Delta t} \right]^2 - \frac{V[x(t_j)] + V[x(t_{j+1})]}{2} \right\} \Delta t.$$
This is nothing but the discrete form of the *classical action* of the
path! Taking the limit $\Delta t \rightarrow 0$ we obtain
$$\varphi =\frac{1}{\hslash} \int_{t_0}^{t_1} \left[ \frac{m \dot{x}^2}{2} - V(x) \right] \; dt =
\frac{1}{\hslash} \int_{t_0}^{t_1} L(x, \dot{x}) \; dt.$$ We therefore
conclude that the probability to go from ${\bf r}_0$ at time $t_0$ to
${\bf r}_1$ at time $t_1$ is given by
$$P({\bf r}_0, t_0; {\bf r}_1, t_1) =  \left| {\mathcal N} \sum_{\text{all paths}} \exp \left[
\frac{{\rm i}}{\hslash} \int_{t_0}^{t_1} L(x, \dot{x}) \; dt \right] \right|^2$$
where ${\mathcal N}$ is the normalisation factor
$${\mathcal N} = \sqrt{\frac{m}{2\pi {\rm i}\Delta t\hslash}}.$$ This
now is the path integral formulation of quantum mechanics. Let us spend
a moment to study this formulation. First note the large prefactor
$1/\hslash$ in front of the exponent. If the phase factor varies when
varying the path, this large prefactor will cause the exponential to
vary wildly over the unit circle in the complex plane. The joint
contribution to the probability will therefore become very small. If on
the other hand there is a region in phase space (or 'path space') where
the variation of the phase factor with the path is zero or very small,
the phase factors will add up to a significant amount. Such regions are
those where the action is stationary, that is, we recover the
*classical* paths as those giving the major contribution to the phase
factor. For $\hslash
\rightarrow 0$ (the classical case), only the stationary paths remain,
whereas for small $\hslash$, small fluctuations around these paths are
allowed: these are the quantum fluctuations.

You may not yet recognise how this formulation is related to the
Schrödinger equation. On the other hand, we may identify the expression
within the absolute signs in the last expression for $P$ with a matrix
element of the time evolution operator since both have the same meaning:
$$\left\langle x_1 \right| \hat{U}(t_1-t_0) \left| x_0 \right\rangle=  \sum_{\text{all paths}} {\mathcal N}\exp \left[
\frac{{\rm i}}{\hslash} \int_{t_0}^{t_1} L(x, \dot{x}) \; dt \right].$$
This form of the time evolution operator is sometimes called the
*propagator*. Let us now evaluate this form of the time evolution
operator acting for a small time interval $\Delta t$ on the wave
function $\psi(x,t)$: 
$$\begin{gathered}
\psi(x_1, t_1) = \int \left\langle x_1 \right|\hat{U}(t_1-t_0) \left| x_0 \right\rangle\left\langle
x_0 | \psi \right\rangle dx_0 = \\ {\mathcal N} \int {\rm D} [x(t)] \int_{-\infty}^\infty 
\exp\left\{ \frac{{\rm i}}{\hslash} \int_{t_0}^{t_1} \left[ m \frac{\dot{x}^2(t)}{2} - 
V[x(t)]\right] \; dt \right\} \psi(x_0, t_0) \; dx_0 .
\end{gathered}$$
The notation $\int {\rm D} [x(t)]$ indicates an
integral over all possible paths from $(x_0, t_0)$ to $(x_1, t_1)$. We
first approximate the integral over time in the same fashion as above,
taking $t_1$ very close to $t_0$, and assuming a linear variation of
$x(t)$ from $x_0$ to $x_1$:
$$\psi(x_1, t_1) = {\mathcal N} \int_{-\infty}^\infty 
\exp\left\{ \frac{{\rm i}}{\hslash} \left[ m \frac{(x_1 - x_0)^2}{2\Delta t^2} - 
\frac{V(x_0)+V(x_1)}{2} \right] \; \Delta t \right\} \psi(x_0, t_0) \; dx_0.$$
A similar argument as used above to single out paths close to stationary
ones can be used here to argue that the (imaginary) Gaussian factor will
force $x_0$ to be very close to $x_1$. The allowed range for $x_0$ is
$$(x_1 - x_0)^2 \ll \frac{\hslash\Delta t}{m}.$$ As $\Delta t$ is taken
very small, we may expand the exponent with respect to the $V\Delta t$
term: $$\psi(x_1, t_1) = {\mathcal N}\int_{-\infty}^\infty 
\exp\left[ \frac{{\rm i}}{\hslash} m \frac{(x_1 - x_0)^2}{2\Delta t} \right]
\left[ 1 - \frac{{\rm i}[ V(x_0)+V(x_1)]}{2\hslash} \Delta t\right] \psi(x_0, t_0) \;  dx_0 .$$
As $x_0$ is close to $x_1$ we may approximate
$\frac{ V(x_0)+V(x_1)}{2\hslash}$ by $V(x_1)/\hslash$. We now change the
integration variable from $x_0$ to $u=x_0-x_1$:
$$\psi(x_1, t_1) = {\mathcal N}\int_{-\infty}^\infty 
\exp\left( \frac{{\rm i}}{\hslash} m \frac{u^2}{2\Delta t} \right)
\left[ 1 - {\rm i}/\hslash V(x_1) \Delta t\right] \psi(x_1+u,t_0)\;  du .$$
As $u$ must be small, we can expand $\psi(x)$ about $x_1$ and obtain
$$\psi(x_1, t_1) = {\mathcal N} \int_{-\infty}^\infty 
\exp\left( \frac{{\rm i}m}{\hslash} \frac{u^2}{2\Delta t} \right)
\left[ 1 - \frac{{\rm i}}{\hslash} V(x_1) \Delta t\right] \left[\psi(x_1, t_0)+u
\frac{\partial}{\partial x} \psi(x_1,t_0) +
\frac{u^2}{2}\frac{\partial^2}{\partial x^2} \psi(x_1,t_0)\right] \;  du .$$
Note that the second term in the Taylor expansion of $\psi$ leads to a
vanishing integral as the integrand is an antisymmetric function of $u$.
All in all, after evaluating the Gaussian integrals, we are left with
$$\psi(x_1, t_1) = \psi(x_1, t_0) - \frac{{\rm i}\Delta t}{\hslash} V(x_1) \psi(x_1, t_0) +
\frac{{\rm i}\hslash\Delta t}{2m}
\frac{\partial ^2}{\partial x^2} \psi(x_1,t_0) .$$

Using
$$\frac{\psi(x_1, t_1) - \psi(x_1, t_0)}{\Delta t} \approx \frac{\partial}{\partial
t} \psi(x_1, t_1),$$ we obtain the time-dependent Schrödinger equation
for a particle moving in one dimension:
$${\rm i}\hslash \frac{\partial}{\partial t} \psi(x,t) = \left[ -
\frac{\hslash^2}{2m}\frac{\partial^2}{\partial x^2} + V(x) \right] \psi(x,t).$$

You may have found this derivation a bit involved. It certainly is not
the easiest way to arrive at the Schrödinger equation, but it has two
attractive features;

-   Everything was derived from simple heuristics which were based on
    viewing a particle as a wave and allowing for interference between
    the waves;

-   The formulation shows that the classical path is obtained from
    quantum mechanics when we let $\hslash \rightarrow 0$.