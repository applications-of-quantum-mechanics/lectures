# Green's functions in quantum mechanics

## Introduction

Green's functions are the workhorses of theoretical quantum mechanics.
They are used in many subfields of quantum mechanics because they are
very powerful. Nevertheless, to many researchers, Green's functions
often seem abstract and difficult. Sometimes this is right, but the use
of Green's functions for quantum systems in which the interactions
between particles are not explicitly considered is not so complicated.
And they can be useful even for such noninteracting systems. In this
chapter, we shall explain what the Green's function is and how it can be
used for analysing different types of problems. In
chapter [5](./ch51.md) we shall use Green's functions when discussing
scattering theory.

## Definition of the Green's function

The Green's function of a system described by a Hamiltonian $H$ is
defined as $$\left( z- H \right) G = \mathbb{1}, \label{Eq:FullG}$$
where the right hand side is the unit operator. This operator can have
different forms, depending on the structure of the Hilbert space. If
that space is a finite-dimensional vector space, the unit operator can
be written as the *unit matrix*
$$\mathbb{1} = \left( \begin{array}{cccc}
1 & 0 & \cdots & 0 \\
0 & 1 & \cdots & 0 \\
\vdots & \vdots & \ddots & 0 \\
0 & 0 & \cdots & 1 \end{array}\right).$$ In the case where the Hilbert
space consists of the normalizable complex functions defined on the real
axis, the matrix elements of the unit operator are given in terms of a
delta function
$$\left\langle x | \mathbb{1} | x'\right\rangle= \delta (x-x').$$ For
the Hilbert space of particles moving in 3D, i.e. the space consisting
of normalizable functions ${\mathbb R}3$ (i.e. $L2$-functions) on
${\mathbb R}^3$, the matrix elements are given by the three-dimensional
delta-function $\delta^{(3)}({\bf r}-{\bf r}')$.

The Green's function may seem a rather arbitrary object -- it is not
immediately clear why this function could be useful in any way.
Moreover, it is defined as the inverse of an operator, and that is
usually difficult to find. It is in particular not obvious what
information we could obtain from this inverse, while we could instead
diagonalize $H$ (which is, numerically, equally difficult as inversion).
In order to give some insight into these questions, we must recall an
important result from complex function theory (see the end of chapter
1):
$$\lim_{\epsilon \downarrow 0} \frac{1}{x+{\rm i}\epsilon} = {\mathcal P} \left(\frac{1}{x}\right) - {\rm i}\pi \delta(x).$$
This turns out very useful as can be seen by expanding the Green's
function for a system with a *discrete* spectrum in the basis consisting
of the eigenstates $\left|\phi_n\right\rangle$ of $H$:
$$G(z) = \frac{1}{z-H} = \sum_n \left| \phi_n\right\rangle\frac{1}{z-E_n} \left\langle\phi_n \right|$$
Here, $z$ can in principle be any complex number, but we decide to
choose it close to, but above the real axis. We then obtain the
*retarded Green's function*
$$G^{\text{r}}(E) = \frac{1}{E-H+{\rm i}\eta}$$ where $E$ is real and
$\eta$ is considered to be small and positive. We only give $E$ as an
argument of the Green's function; the superscript 'r' indicates that we
have moved the energy slightly upward (i.e. to the positive imaginary
part) in the complex plane.

We have
$$G^{\text{r}}(E) = \sum_n \left| \phi_n\right\rangle{\mathcal P} \left( \frac{1}{E-E_n}\right) \left\langle\phi_n \right| - {\rm i}\pi \sum_n \left| \phi_n\right\rangle\delta(E-E_n) \left\langle\phi_n \right|.$$
This Green's function is an operator depending on $E$ (or more
generally, on $z$), and we would like to work with a simpler object. We
therefore study the trace of the Green's function. The trace of an
operator is defined as the sum over the diagonal elements of that
operator:
$${\text{Tr}} (\hat{A}) = \sum_n \left\langle\phi_n \left| \hat{A} \right| \phi_n \right\rangle.$$
where the vectors $\left| \phi_n\right\rangle$ form an orthonormal
basis. It can be shown that the trace is independent of the particular
basis chosen -- for the Green's function, we take the basis consisting
of the eigenstates of the Hamiltonian and find:
$${\text{Tr}} G(z) = \sum_n \frac{1}{z-E_n}.$$ We see that the trace of
the Green's function has a simple pole on the real axis at every energy
eigenvalue $E_n$.

We have learned two important things: (i) the trace of the Green's
function is a complex function which has poles on the real axis which
correspond to the eigenvalues of $H$ and (ii) at these poles the
imaginary part of the Green's function (not its trace) is proportional
to $\left| \phi_n \right\rangle\left\langle\phi_n \right|$, which is a
projection operator onto the corresponding eigenstate $\phi_n$. We see
that having the Green's function is equally useful as having the
eigenstates and eigenfunctions of the Hamiltonian. The reason why we
often use Green's functions is that it is often possible to obtain them
for systems for which the Hamiltonian cannot be diagonalised. An example
is formed by a closed rather than an open system, as we shall see below.

We can also conclude that the trace of the imaginary part of the
(retarded) Green's function gives a series of $\delta$-functions, one
for each energy:
$$\lim_{\eta\downarrow 0} {\text{Tr}}\left[ {\text{Im}} G(E+{\rm i}\eta ) \right] = -\pi \sum_n \delta(E-E_n),$$
where $E$ is taken real. This is an example of a general result which
says that the negative imaginary part of the trace of the Green's
function, multiplied by $1/\pi$, is the *density of states* of a system.

The Green's function is often powerful in studying quantum systems, as
we already mentioned in the introduction. To be specific, (i) the
Green's function is useful for working out perturbation series, (ii) it
plays a major role in scattering theory (again when scattering is
formulated as a pertubative problem) and (iii) the Green's function has
a *local* character: we can evaluate it for a particular region, and it
encodes the influence which this region has on adjacent regions. In this
chapter we shall briefly go into these applications of Green's
functions.

## Green's functions and perturbations

There exists a very important equation that is quite simple but turns
out very powerful for perturbative problems. To obtain this equation,
let us first formulate a perturbative quantum problem by splitting its
Hamiltonian as $$H = H_0 + V,$$ where $V$ is 'small' in some sense.
Usually we mean small with respect to the typical distance between the
energy eigenvalues of $H_0$ or, in the case of a continuous spectrum,
small with respect to the typical eigenenergy of $H_0$ measured with
respect to the ground state energy. We define $G_0$ as the Green's
function of the unperturbed Hamiltonian $H_0$:
$$(z-H_0) G_0 = \mathbb{1}, \label{Eq:DefG0}$$ where the unit operator
on the right hand side is the same as above, i.e. its form depends on
the Hilbert space of the system.

Now it is very easy to obtain the following result:
$$z-H = G^{-1} = z - H_0 - V = G_0^{-1} - V.$$ Multiplying the second
and the fourth form of this equation from the *left* with $G_0$ and from
the *right* with $G$, we obtain, after some reorganisation:
$$G = G_0 + G_0 V G. \tag{B.1}\label{Eq:Dyson1}$$ It seems that we have not made
much progress, as this is an implicit equation for the Green's function
$G$. However, $V$ is small, and this inspires us to take the expression
for $G$ and plug it into the right hand side of this equation. We then
obtain: $$G = G_0 + G_0 V G_0 + G_0 V G_0 V G.$$ The second term on the
right hand side contains one $V$, and the third terms contains *two*
$V$'s. This means that for small $V$, the third term is a lot smaller
than the second one, and if we neglect this very small term, we obtain
an *explicit* equation for $G$: $$G = G_0 + G_0 V G_0.$$ We can iterate
further and further, each time replacing the $G$ on the right hand side
by the full right hand side, and thus obtain an infinite perturbation
series: $$\tag{B.2}\label{Eq:BornS}
G = G_0 + G_0 V G_0 + G_0 V G_0 V G_0 +  G_0 V G_0 V G_0V G_0+\ldots$$
The terms on the right hand side contain increasingly higher-order
contributions in $V$ to the unperturbed Green's function.
Eq. \eqref{Eq:Dyson1} is the famous *Dyson equation*. It is a very
important equation which is used in many fields of physics.
Eq. \eqref{Eq:BornS} is called the *Born series*. Cutting this of after
the first-order term (in $V$) is called the *first Born approximation*,
after the second order term in $V$ it is the *second Born
approximation*, etcetera.

## Systems with continuous spectra

Now we turn to a system with a continuous spectrum. The operator $H_0$
is chosen such as to be easily diagonalizable:
$$H_0 \left| \phi_k \right\rangle= E_{k} \left| \phi_k \right\rangle,$$
where $k$ is now a continuous index.

We take the same energy $E_k$ and define the eigenstate of the full
Hamiltonian at that energy $\left| \psi_k\right\rangle$:
$$H \left| \psi_k \right\rangle= E_{k} \left| \psi_k \right\rangle.$$
Note that we assume that the energy $E_k$ is in the continuous spectrum
of both the unperturbed and the perturbed Hamiltonian. This is the case
for systems which are of interest to us.

The unperturbed Hamiltonian $H_0$ was assumed to be diagonalized, and we
anticipate that in general the $\left| \psi_k\right\rangle$ are
difficult to find. We use perturbation theory for this problem. We can
write the last equation in the form
$$(E_k - H_0) \left| \psi_k \right\rangle= V \left| \psi_k \right\rangle.$$
and combine this with $$(E_{k} - H_0) \left| \phi_k \right\rangle= 0.$$
Now we can write
$$\left| \psi_k  \right\rangle= \left|\phi_k\right\rangle+ (E_k-H_0)^{-1} V \left| \psi_k \right\rangle.$$
(From now on, we leave out the subscript $k$ to the wave functions
$\psi$ and $\phi$.) We have obtained an implicit equation for the wave
function $\left|\psi \right\rangle$, similar to the Dyson equation found
in the previous section. We recognize the Green's function of the
unperturbed system as the operator in front of $V$ on the right hand
side of this equation:
$$\left| \psi  \right\rangle= \left|\phi\right\rangle+ G_0 V \left| \psi \right\rangle.$$
Note that the second term on the right hand side is the perturbation of
the wave function $\left| \phi\right\rangle$ at a *fixed* eigenenergy
due to the presence of the perturbation $V$. Similar to the approach
taken for the Dyson equation, we can try to solve this equation
iteratively. The lowest order approximation is by replacing $\psi$ on
the right hand side by $\phi$:
$$\left| \psi  \right\rangle= \left|\phi\right\rangle+ G_0 V \left| \phi \right\rangle.$$
This is the so-called *first Born approximation* often abbreviated as
'Born approximation'. We may also replace the full expression for $\psi$
into the equation and obtain:
$$\left| \psi  \right\rangle= \left|\phi\right\rangle+ G_0 V \left| \phi \right\rangle+ G_0 V G_0 V \left| \psi \right\rangle.$$
etcetera. This expression just given is the second Born approximation.
This Born approximation is frequently used in scattering theory.
