---
title: The WKB approximation
---

# 4.1. A particle moving in a potential

!!! pull "Expected prerequisites"

    Before the start of this lecture, you should be able to:

    - Describe the motion of a classical particle in a potential.

!!! goals "Learning goals"

    After this lecture you will be able to:

    - Describe the motion of a quantum mechanical wave packet in a smooth or abrupt potential.

## 4.1.1. Classical intuition

The WKB approximation is used to describe a particle of mass $m$ in a one-dimensional (1D) potential with some initial velocity. To call on some intuition, let us recall the classical case: consider a particle of energy $E$ moving in a potential $V(x)$. In this situation, the total energy of the particle is conserved, and can be separated at every point $x$ into kinetic and potential energy as $E = E_\text{kin}(x) + V(x)$.

If $E > V(x)$, the particle will always have an energy higher than the potential at any point $x$. Hence, there will always be some non-zero kinetic energy at every point $x$. As a consequence, the particle will continuously move in one direction (determined by the initial velocity). This happens irrespective of the potential shape: for example, it does not matter whether the potential is smooth or changes abruptly.

![Classical particle with energy smaller than potential energy](figures/classical_scat1.svg)

If $E < V(x)$, a classical particle will reach a maximum position $x_0$ such that $E = V(x_0)$ and the kinetic energy becomes zero. Then, the particle will be reflected. Again, this will happen for any potential shape.

![Classical particle with energy larger than potential energy](figures/classical_scat2.svg)

In summary, the global motion of a particle in classical mechanics in a 1D potential is determined only by the value of the total energy compared to the potential $V(x)$.

## 4.1.2 Quantum case

Let us now consider the quantum case: instead of a point particle, we will use its quantum analogue, a wave packet, and let it time-evolve in different potentials.

### Smooth potential

Firstly, we consider the case where $V(x)$ is a smooth potential describing a dip. One can confirm with the animation below that the intuition from classical mechanics still holds because the particle moves across the potential with a sightly increased velocity while in the dip.


```python inline
import common
from IPython.display import HTML, display
common.configure_plotting()
from wkb import make_wave_packet_animation
import math

def pot_gauss(x):
    return -0.02*math.exp(-(x-1500.0)**2/200**2)

anim = make_wave_packet_animation(L=3500, pot_func=pot_gauss, 
                                  zero_pos=750, width=100,
                                  energy=0.01)

display(HTML(anim.to_jshtml(default_mode='loop')))
```

### Abrupt potential

One the other hand, if $V(x)$ describes an abrupt potential, one can observe in the following animation that the wave function reflects back whenever it crosses an abrupt step.


```python inline
import common
from IPython.display import HTML, display
common.configure_plotting()
from wkb import make_wave_packet_animation
import math

def pot_step(x):
    if 1500 < x < 2200:
        return -0.02
    else:
        return 0

anim = make_wave_packet_animation(L=3500, pot_func=pot_step, 
                                  zero_pos=750, width=100,
                                  energy=0.01)

display(HTML(anim.to_jshtml(default_mode='loop')))
```

This effect is not present in the classical case.

## 4.1.3. Summary 

!!! note "Take-home message"

    For a quantum particle with $E > V(x)$:

    * When a particle moves along a smooth potential, it resembles the classical case.
    * When a particle moves along an abrupt potential, there's always some reflection.