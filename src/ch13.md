# 1.3. Problems

## 1.1  
An electron in a hydrogen atom finds itself in a state
$$\left|\psi \right\rangle= \left| 1, 0, 0 \right\rangle+ \frac{1}{2} \left| 2,1,1\right\rangle+ \frac{1}{\sqrt{2}}
\left| 2, 1, 0 \right\rangle+ \frac{{\rm i}}{\sqrt{2}} \left| 2, 1, -1 \right\rangle.$$
We neglect spin and the states are labeled as
$\left| n, l, m \right\rangle$.

1.  Normalise this state.

2.  Calculate the probability of finding the electron with energy
    $E_2$.

3.  Calculate the probability of finding the electron with angular
    momentum component $L_z=0$.

4.  Calculate the probability of finding $L_x=\hslash$ in a
    measurement.

5.  The hydrogen atom is subject to a field which adds a term
    $\alpha L_x$ to the Hamiltonian. Give
    $\left| \psi(t) \right\rangle$, if the state given above is the
    state at $t=0$.

## 1.2.
Consider a spin-1/2 particle in a spherically symmetric potential.
The state of the particle is denoted $\left|\psi\right\rangle$.
$\bf L$ is the orbital angular momentum and $\bf
S$ the spin operator. The functions $\psi_+$ and $\psi_-$ are
defined by
$$\psi_{\pm}({\bf r}) = \left\langle{\bf r}, \pm | \psi\right\rangle,$$
where the second argument in the bra-vector on the right-hand side
denotes the spin and where $$\begin{align}
\psi_+({\bf r}) = R(r) \left[ Y^0_0 (\theta, \phi) + \frac{1}{\sqrt{3}}
Y^1_0 (\theta, \phi) \right]; \\
\psi_-({\bf r}) = \frac{R(r)}{\sqrt{3}} \left[ Y^1_1 (\theta, \phi) -
Y^1_0 (\theta, \phi) \right];
\end{align}$$ with $R$ some given function of $r$ and
$Y^l_m(\theta,\phi)$ the eigenfunctions of the angular momentum
operators $L^2, L_z$.

1.  Which condition must be satisfied by $R(r)$ in order for
    $\left|\psi\right\rangle$ to be normalised?

2.  A measurement of the spin $z$-component $S_z$ is performed. What
    are the possible results with respective probabilities? Same
    questions for $L_z$ and $S_x$.

3.  A value 0 is measured for the quantity $L^2$. What is the state
    of the particle immediately after the measurement?

## 1.3.
An electron subject to a magnetic field in the $z$ direction evolves
under the Hamiltonian $$H=-\frac{1}{2}\Delta_1 S_z.$$ At $t=0$, the
electron spin points along the positive $x$ direction.

1.  Calculate the time-dependent wavefunction.

2.  Formulate the equations of motion of the expectation values of
    the spin components, $\left\langle S_x \right\rangle$,
    $\left\langle S_y \right\rangle$ and
    $\left\langle S_z \right\rangle$.

From now, we consider the stationary behaviour rather than the
dynamics.

3.  Consider now a second electron. This second electron experiences
    a field in the $z$ direction, but with different magnitude. The
    two electrons interact via a weak transverse coupling. The total
    Hamiltonian is thus given by:
    $$H=-\frac{1}{2}\Delta_1 S_{z1}-\frac{1}{2}\Delta_2 S_{z2} + J\left( S_{x1} S_{x2}+ S_{y1} S_{y2}\right),$$
    where $J<<\Delta_1,\Delta_2,|\Delta_1-\Delta_2|$.

    Use first-order perturbation theory to estimate the energies and
    corresponding eigenstates for this Hamiltonian.

4.  Use second-order perturbation theory to estimate the energies
    and corresponding eigenstates for this Hamiltonian.

5.  Find the exact solution for the energies and eigenstates.

## 1.4.  Two spins

Consider two spin-1/2
particles subject to a Heisenberg-type interaction
$$H_0 = -\alpha {\bf S}_1 \cdot {\bf S}_2,$$ where ${\bf S}_1$ and
${\bf S}_2$ are the spin operators for particle 1 and particle 2,
respectively.

1.  Find the energies and expand the corresponding eigenstates in
    the basis $\left| s, m_s \right\rangle$, where $s$ and $m_s$
    denote the quantum numbers for the total momentum operators
    ${\bf S}= {\bf S}_1 + {\bf S}_2$ and
    ${S}_z= {S}_{z1} + {S}_{z2}$, respectively.

2.  Expand the eigenstates in the basis
    $\left| m_{s1}, m_{s2}\right\rangle$, where $m_{s1}$ en $m_{s2}$
    are the quantum numbers of the operators $S_{z1}$ en $S_{z2}$.

3.  Consider now the modified Hamiltonian $$H = H_0 + \beta S_z^2,$$
    Give the exact energies and corresponding eigenstates.

4.  Determine whether the ground state of the system is entangled.
    Note: an entangled state cannot be written as a product of
    states for particle 1 and particle 2.

## 1.5.
Consider two quantum dots in close proximity. A quantum dot is a
small structure that can be occupied by an electron. We consider the
case where only one state is available in each dot. In this problem,
we assume that exactly one electron is present in the system. Denote
the energies of the states as $\epsilon_i$, $i=1, 2$ where $i$
labels the dot. To these levels correspond the states
$\left| \phi_i\right\rangle$, $i=1, 2$. As the dots are placed close
together, they are coupled. The coupling constant is (the complex
number) $\tau$:
$$\tau = \left\langle\phi_1 \left| H \right| \phi_2\right\rangle,$$
where $H$ is the Hamiltonian.

1.  Write the Hamiltonian of the system in the form of a $2\times 2$
    matrix.

2.  Find the spectrum of this Hamiltonian. Plot the spectrum as a
    function of $\epsilon_1 - \epsilon_2$ for fixed $\tau$. Also
    find the eigenfunctions.

3.  Suppose we place an electron in dot 1 at $t=0$. Give the time
    evolution of the wave function and show that the probability to
    find the electron in dot 2 as a function of time has the form
    $$P(t) = C \left[ 1-\cos(\omega t)\right].$$ Determine $\omega$.
    Hint: write the quantum state $\left| \psi \right\rangle$ as
    $$\left| \psi\right\rangle= a \left| \phi_1 \right\rangle+ b \left| \phi_2\right\rangle$$
    and use the spectrum found in (b) together with the
    time-dependent solution of the Schrödinger equation.

## 1.6.
A particle is located at the origin of a line where the potential
$V$ is zero. At $t=0$, the particle is released. Find the wave
function in the $x$-representation at time $t>0$.

## 1.7.  

1.  Consider the three spin triplet states
    $\left| 1 m \right\rangle$ (in the standard notation
    $\left| s, m_s\right\rangle$) and the singlet state
    $\left| 0 0 \right\rangle$, which are constructed out of two
    spin-$\frac{1}{2}$ particles, A and B. The vector
    $\boldsymbol{ \sigma}_{\text{A}} = 
    ( \sigma_{{\text{A}}x}, \sigma_{{\text{A}}y}, \sigma_{{\text{A}}z})$
    has the three Pauli spin matrices as its components, and
    $\boldsymbol{ \sigma}_{\text{B}}$ is defined in a similar way.
    Show that
    $$\left( \boldsymbol{ \sigma}_{\text{A}} \cdot \boldsymbol{ \sigma}_{\text{B}} \right) \left| 1 m \right\rangle= + \left| 1 m \right\rangle$$
    and
    $$\left( \boldsymbol{ \sigma}_{\text{A}} \cdot \boldsymbol{ \sigma}_{\text{B}} \right) \left| 0 0 \right\rangle= -3 \left| 0 0 \right\rangle.$$
    Obtain the eigenvalues of
    $\left( \boldsymbol{ \sigma}_{\text{A}} \cdot \boldsymbol{ \sigma}_{\text{B}} \right)^n$.

2.  A system consisting of two spin $\frac{1}{2}$ particles is
    described by the Hamiltonian
    $$H = \begin{cases} \lambda \left( \boldsymbol{ \sigma}_{\text{A}} \cdot \boldsymbol{ \sigma}_{\text{B}} \right), & t\geq 0, \\
                                0 & t<0.
            \end{cases}$$ Assume
    $\left|\psi(t=0)\right\rangle= \left| \downarrow \uparrow \right\rangle$,
    i.e. particle A has spin along the $-z$ axis, and particle B has
    its spin oriented along the $+z$ axis. Express $H$ in terms of
    the vector operator
    $\boldsymbol{ \sigma} = \boldsymbol{ \sigma}_{\text{A}} + \boldsymbol{ \sigma}_{\text{B}}$.
    Obtain $\left| \psi(t) \right\rangle$ for $t>0$ and determine
    the probability for finding the system in the states
    $\left| \uparrow \uparrow\right\rangle$,
    $\left| \downarrow\downarrow \right\rangle$ and $\left| \uparrow
    \downarrow \right\rangle$.

## 1.8.
We consider some properties of the Pauli matrices $\sigma_x$,
$\sigma_y$ and $\sigma_z$.

1.  Show that $\sigma_i^2=1$, for $i=x, y, z$.

2.  Show that
    $[\sigma_i, \sigma_j] = 2 {\rm i}\epsilon_{ijk} \sigma_k$
    where $\epsilon_{ijk} = 1$
    whenever $i,j,k$ is an *even* permutation of $x, y, z$ and
    $\epsilon_{ijk}=-1$ when
    $i,j,k$ is an *odd* permutation of $i,j,k$.

3.  For angular momentum quantum number $l=1$, we have three
    possible states $\left| 1, m\right\rangle$, where $m=1, 0, -1$.\
    Write down the matrix form of $L_z$ in the basis of these
    states.

4.  Show that the matrices
    $$L_x = \frac{\hslash}{\sqrt{2}} \left( \begin{array}{ccc} 0 & 1 & 0 \\ 1 & 0 & 1 \\ 0 & 1 & 0 \end{array}\right) {\text{ and }}
    L_y = \frac{\hslash}{\sqrt{2}} \left( \begin{array}{ccc} 0 & -{\rm i}& 0 \\ {\rm i}& 0 & -{\rm i}\\ 0 & {\rm i}& 0 \end{array}\right),$$
    together with the matrix found in (c), satisfy the angular
    momentum commutation relations
    $$\left[ L_i, L_j \right] ={\rm i}\hslash \epsilon_{i,j,k} L_k.$$

5.  Calculate the eigenvalues and eigenvectors of $L_x$ and $L_y$.

1.9.  *Bloch* 

*It may be useful to refer to the previous problem to refresh your knowledge concerning the Pauli
matrices!* 

The state of a quantum two-level system (also called quantum bit,
qubit or Q-bit) can be written as
$$|\psi\rangle=\alpha\; |0\rangle +\beta\; |1\rangle,$$ where the
coefficients $\alpha$ and $\beta$ are complex, and properly
normalised: $|\alpha|^2+|\beta|^2=1$.

1.  Explain why, without loss of generality, we can also write the
    state as
    $$|\psi\rangle=\cos(\theta/2)\; |0\rangle +e^{{\rm i}\phi}\sin(\theta/2)\; |1\rangle,$$
    with $\theta$ ranging from 0 to $\pi$ and $\phi$ ranging from 0
    to $2\pi$.

2.  There is a one-to-one correspondence between the state
    $|\psi\rangle$ and a unit vector in 3-D, whose orientation is
    defined by polar and azimuthal angles $\theta$ and $\phi$,
    respectively (see
    figure [(1.1)](./ch11.md#Fig:BlochSph). Such a vector is called the *Bloch
    vector* associated with $|\psi\rangle$. Draw the Bloch vectors
    associated with the states $|0\rangle$, $|1\rangle$,
    $\frac{1}{\sqrt{2}}|0\rangle + \frac{1}{\sqrt{2}}|1\rangle$, and
    $\frac{1}{2}|0\rangle - \frac{i\sqrt{3}}{2}|1\rangle$.

3.  Find $\langle\psi|\sigma_x|\psi\rangle$,
    $\langle\psi|\sigma_y|\psi\rangle$, and
    $\langle\psi|\sigma_z|\psi\rangle$ as a function of $\theta$ and
    $\phi$. Here, $\sigma_x$, $\sigma_y$ and $\sigma_z$ are the
    Pauli matrices.

4.  Show that
    $\langle \sigma_x\rangle^2+ \langle \sigma_y\rangle^2+ \langle \sigma_z\rangle^2=1$.

5.  How are $\langle \sigma_x\rangle$, $\langle \sigma_y\rangle$ and
    $\langle \sigma_z\rangle$ related to the cartesian coordinates
    of the Bloch vector?

6.  Unitary operations on one qubit correspond to rotations in the
    *Bloch sphere*. What kind of rotations correspond to the
    operators $\sigma_x$, $\sigma_y$ and $\sigma_z$? Specify the
    axis of rotation, and the rotation angle for each.

7.  What does the operator
    $\hat{R}_{x}(\alpha)\equiv \cos(\alpha/2) \unicode{120793} - {\rm i}\sin(\alpha/2)\sigma_x$
    do?

8.  What does the operator
    $\hat{R}_{\vec{n}}(\alpha)\equiv \cos(\alpha/2) \unicode{120793} - {\rm i}\sin(\alpha/2)\vec{n} \cdot \{\hat{\sigma}_x,\hat{\sigma}_y,\hat{\sigma}_z\}$
    do? Here, $\vec{n}$ is a 3-D unit vector.

    (You only need to show that $\hat{R}_{\vec{n}}$ leaves the state
    with Bloch vector $\vec{n}$ invariant.)

## 1.10. Gauge 

The non-relativistic
Hamiltonian that describes the interaction of a charged particle
with the electromagnetic field is
$$H=\frac{1}{2m} \left( {\bf p}- \frac{q}{c} {\bf A}({\bf r})\right)^2 + q \phi({\bf r})$$

1.  Assume that the wave function is changed by a constant phase
    $$\psi({\bf r},t) \rightarrow \psi'({\bf r},t) = e^{{\rm i}\alpha} \psi({\bf r}, t).$$
    Show that $\psi'({\bf r},t)$ satisfies the original Schrödinger
    equation with the original vector potential
    ${\bf A}({\bf r},t)$.

2.  Assume that the wave function is changed by a *non-constant*
    phase
    $$\psi({\bf r},t) \rightarrow \psi'({\bf r},t) = e^{{\rm i}\alpha({\bf r},t)} \psi({\bf r}, t).$$
    Show that $\psi'$ *does not* satisfy the original Schrödinger
    equation with the original vector potential
    ${\bf A}({\bf r},t)$.

3.  Show that $\psi'({\bf r},t)$ *does* satisfy the original
    Schrödinger equation  but with a new vector potential
    $${\bf A}'({\bf r},t)  = {\bf A}({\bf r},t) + \boldsymbol{\chi}({\bf r},t),$$
    where $\boldsymbol{\chi}$ is a vector field. How is the gauge
    term $\boldsymbol{\chi}({\bf r}, t)$ related to the phase term
    $\alpha({\bf r}, t)$?\
    How does the scalar potential change:
    $$\phi({\bf r},t) \rightarrow \phi'({\bf r},t) = \phi({\bf r},t) +?$$


