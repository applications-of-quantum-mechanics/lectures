---
title: WKB Approximation - Transport
---

# 4.5. Transport

!!! pull "Expected prerequisites"

    Before the start of this lecture, you should be able to:

    1. Describe the behaviour of tunnelling through a rectangular barrier
    2. Write down the WKB wave function
    3. Apply the connection formulas to connect the WKB wave functions across a turning point
    4. Rewrite integrals

!!! goals "Learning goals"

    After this lecture you will be able to:

    1. Write down the transmission probability of a tunnelling problem.
    2. Describe the effect of the potential on the probability current for a travelling wave.

## 4.5.1. Setting up a transport problem

Having looked into bound states, we will now focus on quite the opposite situation: 

- Far away from the potential, where the energy is *larger* than the potential, 

$$
\text{i.e. } E>V(x) \quad \text{for} |x|\rightarrow \infty 
$$

In such an *open* case, we will not have bound states but there will be eigenstates for a continuous range of energies. Far away from some central potential, the eigenstates can be written as a superposition of plane waves. One particular choice for the eigenstate is to choose to have only an incoming wave on one side (this is always possible) and to consider its reflection and transmission as shown in the following sketch:

![tunneling](figures/tunneling.svg)

Assuming that the potential becomes constant for some point away from the central potential, the wave function can be written as 
$$
\psi(x) = \left\{\begin{array}{cc}
A e^{ik_L x} + B e^{-ik_L x}, & x \rightarrow -\infty\\
F e^{ik_R x}, & x \rightarrow \infty
\end{array}\right.
$$
where 
$$
\begin{align}
k_L &= \sqrt{\frac{2m}{\hbar^2} (E-V(x\rightarrow -\infty))} \\
k_R &=\sqrt{\frac{2m}{\hbar^2} (E-V(x\rightarrow \infty))}
\end{align}
$$ 
are the wave vectors on the left and right side. These can be different if the potential left and right are not equal.

In such a setting, the interesting question is: 

*How much of the incoming wave is transmitted to the right, and how much reflected to the left?* 

In fact, you solved exactly such a system already in your first quantum mechanics class when you computed the tunnel probability through a square barrier! With WKB, we can now find approximate solutions for any kind of potential.

The central quantity of interest is the tunneling probability $T$. As in the example of the square tunnel barrier, it is generally defined as
$$
T = \left| \frac{F}{A} \right|^2 \frac{v_R}{v_L},
$$
where $v_L$ and $v_R$ are the velocities to the left and the right of the central potential. The reasoning behind the velocity entering the equation is that transmission probability corresponds to the probability *current* and the question of how much of the incident current goes through and how much is reflected.

It is possible to get rid of the awkward velocity term by a small redefinition: We can instead write:
$$
\psi(x) = \left\{\begin{array}{cc}
\frac{A}{\sqrt{v_L}} e^{ik_L x} + \frac{B}{\sqrt{v_L}} e^{-ik_L x} & x \rightarrow -\infty,\\
\frac{F}{\sqrt{v_R}} e^{ik_R x}, & x \rightarrow \infty.
\end{array}\right.
$$
The point is that with this convention, the waves to the left and right have a constant current if e.g. the coefficients $A$ and $F$ are the same. In fact, we can immediately see that for the WKB wave functions we have already chosen such a normalization where $\frac{1}{\sqrt{p(x)}} \sim \frac{1}{\sqrt{v}}$:
$$
\psi(x)_{E > V(x)} \sim  \frac{A}{\sqrt{p(x)}}e^{\frac{i}{\hbar} \int_{x_0}^{x} p(x') dx'} + \frac{B}{\sqrt{p(x)}}e^{-\frac{i}{\hbar} \int_{x_0}^{x} p(x') dx'}\,.
$$

The tunnel probability is thus simply given by
$$
T = \left| \frac{F}{A}\right|^2\,.
$$

## 4.5.2. $E > V(x)$: Reflectionless transport 

Let us start with the simplest case, where $E > V(x)$ for all $x$. For that case we already previously wrote down the [WKB wave function](wkb_wf.md) for all $x$. In particular, if we only consider an incoming wave from the left, we obtain
$$
\psi_\text{WKB}(x) \sim \frac{1}{\sqrt{p(x)}}e^{\frac{i}{\hbar} \int_{x_0}^{x} p(x') dx'}\,.
$$
We see here that there is no reflected wave, so the transmission probability is $T=1$. Indeed, we argued initially that for a smooth potential there is no backreflection, so no surprises here!

It can still be interesting to think about how the wave function interacts with the potential. When [deriving the WKB wave function intuitively](wkb_wf.md#422-intuitive-derivation), we used current conservation to derive the amplitude of the wave function. In particular,

$$
j \sim A(x)^2 \frac{p(x)}{m}\,,
$$

where $A(x)$ was the amplitude of the wave function. We thus see that for regions:

-   where the momentum $p(x)$ decreases, i.e. for larger $V(x)$, the amplitude needs to increase. 
-   Conversely, when $p(x)$ increases, the amplitude needs to decrease. 

This phenomenon can be observed in the following animation:

```python inline
import common
from IPython.display import HTML, display
common.configure_plotting()
from wkb import wkb_static_animation
import numpy as np

x = np.linspace(500, 2500, 1000)
anim = wkb_static_animation(x, E=1.5)
display(HTML(anim.to_jshtml(default_mode='loop')))
```

## 4.5.3. Intuitive derivation for the tunneling case

The more interesting case is when there is a central part where $V(x) > E$, because then we have a finite reflection and a transmission $T<1$.

In this section, we will first give an intuitive, not rigorous derivation of the tunnelling probability using WKB approximation.

Let us first consider the familiar case of a rectangular barrier. We know that in this case, the wave function under the barrier consists of both an exponentially decaying and an exponentially growing part: $\psi(x) = C e^{-\kappa x} + D e^{+\kappa x}$. In the following interactive picture, you can explore how the wave function changes as a function of barrier height:

```python inline
import common
from IPython.display import HTML, display
common.configure_plotting()
from wkb import tunnel_animation
import numpy as np

def pot_step(A=0.1):
    def V(x):
        if 40 < x < 60:
            return A
        else:
            return 0
    return V

mags = np.linspace(0.035, 0.1, 20)
anim = tunnel_animation(mags=mags, potential_f=pot_step)
display(HTML(anim.to_jshtml(default_mode='loop')))
```

We observe that as the barrier gets higher, the wave function in the central part becomes more and more dominated by the exponentially decaying part, $\psi(x) \sim C e^{-\kappa x}$. Even more, the wave function to the right is suppressed due to this dominance of the decaying part. In a good approximation, the wave function to the right of the barrier is then suppressed by $e^{-\kappa L}$, where $L$ is the barrier length. Consequently, we can estimate the tunnel probability to be

$$
T \sim \left(e^{-\kappa L}\right)^2 = e^{-2 \kappa L}\,.
$$

We can use the same argument now for a tunnel barrier that has some smooth variation instead of a constant height:

!!! success "Intuitively derived tunnelling probability"

    To solve for the wavefunction in the region $x_1=0 < x < x_2=L$, we can use the WKB approximation. To account for an arbitrary potential shape, recall that the WKB wavefunction is,
    $$
    \psi_{WKB}(x) \sim \frac{1}{\sqrt{|p(x)|}} \left( C e^{ -\frac{1}{\hbar} \int_{0}^x |p(x')| d x'} + D e^{ \frac{1}{\hbar} \int_{0}^x |p(x')| d x'} \right).
    $$
    In the limit of a high barrier and/or long barrier, we can again approximate the wave function under the barrier by only the decaying part:
    $$
    \psi_{WKB}(x) \sim \frac{C}{\sqrt{|p(x)|}} e^{ -\frac{1}{\hbar} \int_{0}^x |p(x')| d x'}\,.
    $$
    Hence, the WKB tunneling probability is
    $$
    T = \sim e^{-2\gamma},\qquad \gamma = \frac{1}{\hbar}\int_0^L |p(x')|dx'. \tag{4.9}\label{eq:wkb_tunnel}
    $$
    This formula is in fact one of the most famous results of the WKB formulation, and very useful to estimate tunneling probablities


## 4.5.4. Derivation of the WKB tunneling probability for a smooth barrier

The previous derivation lacks mathematical rigour. Using the connection formulas, we *can* actually solve the full problem in WKB approximation - and, as we will see below - we will exactly recover the WKB tunneling probability \eqref{eq:wkb_tunnel} in the limit of a high and wide barrier.

To this end, we will use the transfer matrix method. 

Let us recall that the WKB wave function for propagating waves is,

$$
\psi(x) = \frac{1}{\sqrt{p(x)}} \left( A e^{\frac{i}{\hbar} \int_{x}^{x_1} p(x') d x'} + B e^{-\frac{i}{\hbar} \int_{x}^{x_1} p(x') d x'}\right).
$$

In order to use the connection formulas, $\psi$ must be expressed in the sine and cosine basis with coefficients $\tilde A$ and $\tilde B$. The correspondence   between the coefficients around the turning points is shown in the plot below.


```python inline
import common
common.configure_plotting()
import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-1, 1, 100)
y1 = x
y2 = -x
E = 0
fig, (ax1, ax2) = plt.subplots(1,2, sharex=True, sharey=True,  figsize=(10,4))
ax1.plot(x, y1, color='black', label=r'V(x)')
ax2.plot(x, y2, color='black', label=r'V(x)')


for ax in ax1, ax2:
    ax.hlines(y=E, xmin=-1, xmax=1, color='red', label='E')
    ax.set_xticks([]);
    ax.set_yticks([]);
    ax.vlines(x=0, ymin=-2, ymax=0, linestyle='dashed', color='black')
    ax.scatter(0, 0, color='black', zorder=100)
    ax.set_ylim(-1, 1);
    ax.legend(fontsize=15)

ax1.set_xlabel(r'$x_1$', fontsize=15)
ax1.text(-0.5, E+0.1, r'$\tilde A$  $\tilde B$', fontsize=18, color='darkblue')
ax1.text(0.2, E-0.2, r'$C_1$  $D_1$', fontsize=18, color='darkblue')
ax2.set_xlabel(r'$x_2$', fontsize=15)
ax2.text(-0.8, E-0.2, r'$C_2$  $D_2$', fontsize=18, color='darkblue')
ax2.text(0.2, E+0.1, r'$\tilde E$  $\tilde F$', fontsize=18, color='darkblue')

fig.show()
```



1. To the left of the turning point $x_1$, the wavefunction is given as, 
    
    $$
    \psi(x)_{x < x_1} =  \frac{1}{\sqrt{p(x)}} \left[\tilde A \sin\left( \frac{1}{\hbar} \int_{x}^{x_1} p(x') d x' + \frac{\pi}{4}\right) + \tilde B \cos \left( \frac{1}{\hbar} \int_{x}^{x_1} p(x') d x' + \frac{\pi}{4}\right)\right]
    $$

    Similarly, to the right of $x_1$,
    $$
    \psi(x)_{x > x_1} =  \frac{1}{\sqrt{\left| p(x) \right|}} \left[ C_1 \exp\left( \frac{1}{\hbar} \int_{x_1}^{x} \left| p(x') \right| d x'\right) + D_1 \exp\left( -\frac{1}{\hbar} \int_{x_1}^{x} \left| p(x') \right| d x'\right)\right].
    $$
        
2. From the connection formulas, we recall that the coefficients are connected as follows,
    $$
    \tilde A = 2 C,\qquad \tilde B = D.
    $$

3. This transformation can be expressed as a transfer matrix, that is,
    $$
    \left( \begin{array}{c}
    \tilde A \\
    \tilde B
    \end{array} \right) = T_2 \left( \begin{array}{c}
    C \\
    D
    \end{array} \right), \quad \text{where }
    T_2 = \left( \begin{array}{cc}
    2 & 0 \\
    0 & 1
    \end{array} \right)
    $$
4. Let us express $\psi_{x< x_1}(x)$ in the plane wave basis. That is,
    $$
    \begin{split}
    \psi_{x< x_1}(x) =& \quad && \frac{\tilde A}{\sqrt{p(x)}} \frac{1}{2 i} \left( e^{ \frac{i}{\hbar} \int_{x}^{x_1} p(x') d x' + \pi/4} - e^{ -\frac{i}{\hbar} \int_{x}^{x_1} p(x') d x' - \pi/4} \right)\\
    &+ &&\frac{\tilde B}{\sqrt{p(x)}} \frac{1}{2} \left( e^{ \frac{i}{\hbar} \int_{x}^{x_1} p(x') d x' + \pi/4} + e^{ -\frac{i}{\hbar} \int_{x}^{x_1} p(x') d x' - \pi/4}\right)\\
    =& \quad && \left( \frac{\tilde A e^{i \pi/4}}{2i} + \frac{\tilde B e^{i \pi/4}}{2} \right) \frac{1}{\sqrt{p(x)}} e^{ \frac{i}{\hbar} \int_{x}^{x_1} p(x') d x'}\\
    &+ && \left( - \frac{\tilde A e^{-i \pi/4}}{2i} + \frac{\tilde B e^{-i \pi/4}}{2} \right) \frac{1}{\sqrt{p(x)}} e^{ -\frac{i}{\hbar} \int_{x}^{x_1} p(x') d x'} \\
    \vdots \;& \\
    \psi_{x< x_1}(x) =& \quad && \frac{1}{\sqrt{p(x)}} \left( A e^{\frac{i}{\hbar} \int_{x}^{x_1} p(x') d x'} + B e^{-\frac{i}{\hbar} \int_{x}^{x_1} p(x') d x'}\right).
    \end{split}
    $$
5. The connection between the two basis can be expressed using a transfer matrix. That is,
    $$
    \left( \begin{array}{c}
    A \\
    B
    \end{array} \right) = T_1 \left( \begin{array}{c}
    \tilde A \\
    \tilde B
    \end{array}\right), \quad \text{where }
    T_1 = \frac{1}{2i} \left( \begin{array}{cc}
    e^{i \pi/4} & ie^{i \pi/4} \\
    -e^{-i \pi/4} & ie^{-i \pi/4}
    \end{array} \right).
    $$

6. The next step is to find the relation between the coefficients $C_1$ and $D_1$ with $C_2$ and $D_2$.
    The wave function around each turning point can be written as,
    $$
    \begin{split}
    \psi(x)_{x > x_1} &=  \frac{1}{\sqrt{|p(x)|}} \left[ C_1 \exp\left( - \frac{1}{\hbar} \int_{x_1}^{x} |p(x')| d x'\right) + D_1 \exp\left( \frac{1}{\hbar} \int_{x_1}^{x} |p(x')| d x'\right)\right],\\
    \psi(x)_{x < x_2} &=  \frac{1}{\sqrt{|p(x)|}} \left[ C_2 \exp\left( - \frac{1}{\hbar} \int_{x}^{x_2} |p(x')| d x'\right) + D_2 \exp\left( \frac{1}{\hbar} \int_{x}^{x_2} |p(x')| d x'\right)\right].
    \end{split}
    $$
7. Let us observe that each of the terms in both wave functions describes either an exponential decay or exponential increase. Then, we match the coefficients that describe the same behaviour. Such consideration relates the coefficients as $C_1 \leftrightarrow D_2$ and $C_2 \leftrightarrow D_1$. That is,
    $$
    \begin{split}
    C_1 \exp\left( - \frac{1}{\hbar} \int_{x_1}^{x} |p(x')| d x'\right) &= D_2 \exp\left( \frac{1}{\hbar} \int_{x}^{x_2} |p(x')| d x'\right)\\
    \longrightarrow C_1 &= D_2 \exp\left( \frac{1}{\hbar} \int_{x_1}^{x_2} |p(x')| d x'\right) = e^\gamma D_2.
    \end{split}
    $$
    A similar relation can be derived for the second pair of coefficients. That is,
    $$
    D_1 = e^{-\gamma} C_2.
    $$
8. Expressing this relation as a transfer matrix, we find,
    $$
    \left( \begin{array}{c}
    C_1 \\
    D_1
    \end{array} \right) = T_3 \left( \begin{array}{c}
    C_2 \\
    D_2
    \end{array}\right), \quad \text{where }
    T_3 = \left( \begin{array}{cc}
    0 & e^{\gamma} \\
    e^{-\gamma} & 0
    \end{array} \right).
    $$

9. At the second turning point, $x_2$, the slope has an opposite sign. By repeating the same derivation as before, one finds that the transfer matrix correspond to the inverse of the previous case.
    That is,

    $$
    \left( \begin{array}{c}
    C_2 \\
    D_2
    \end{array} \right) = T_2^{-1} \left( \begin{array}{c}
    \tilde E \\
    \tilde F
    \end{array}\right).
    $$
    The last,
    $$
    \left( \begin{array}{c}
    \tilde E \\
    \tilde F
    \end{array} \right) = T_1^{-1} \left( \begin{array}{c}
    E \\
    F
    \end{array}\right)
    $$

10. Therefore, the coefficients at both sides of the potential can be related as,
    $$
    \left( \begin{array}{c}
    A \\
    B
    \end{array} \right) = T_1 T_2 T_3 T_2^{-1} T_1^{-1} \left( \begin{array}{c}
    E \\
    F
    \end{array}\right) = \left(
    \begin{array}{cc}
    \frac{i e^{-\gamma }}{4}-i e^{\gamma } & \dots \\
    \dots & \dots\\
    \end{array}
    \right) .
    $$
    Here we have omitted the terms that do not correspond to the tunnelling amplitude. 
    
!!! success "Rigorously derived tunnelling probability" 
    Finally, we can read off the tunnelling probability:
    $$
    \begin{split}
    T = \left| \frac{E}{A}\right|^2 = t^2 &= \frac{1}{|\frac{i e^{-\gamma }}{4}-i e^{\gamma }|^2}\\
    &= \frac{1}{e^{2\gamma} - \frac{1}{2} + \frac{1}{16}e^{-2\gamma}}\\
    &= \frac{e^{-2\gamma}}{1-\frac{1}{2}e^{-2\gamma} + \frac{1}{16}e^{-4\gamma}}
    \end{split}
    $$
    In the limit of a high and/or wide tunnel barrier, $\gamma$ will be large and hence $e^{-2\gamma} \ll 1$. The denominator of the tunnelling probability can thus be approximated as $1-\frac{1}{2}e^{-2\gamma} + \frac{1}{16}e^{-4\gamma} \approx 1$, and we recover the result for the tunnelling probability found in the previous section. That is,
    $$
    T \approx e^{-2\gamma},\qquad \gamma = \frac{1}{\hbar}\int_{x_1}^{x_2} |p(x')|dx'.
    $$

## 4.5.5. Summary

!!! note "Take-home message"

    The following two ideas are the take-home message from this section:

    * The transmission probability in the long and high barrier limit is $T\approx e^{-2\gamma}$.
    * The density of the wave function increases due to current conservation when it is closer to the potential.