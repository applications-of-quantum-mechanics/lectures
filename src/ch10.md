---
title: Introduction
---

# 1. Quantum mechanics survival guide {#Chap:QMIntro}

## 1.0. Review of the standard quantum mechanics
In this chapter, we review standard quantum mechanics, highlighting
important results. Quantum mechanics describes the evolution of the
state of a mechanical system. The state is a vector in a special kind of
complex vector space, the *Hilbert space*. This is a vector space in
which an inner product is defined.

We represent the state of the system by a ket-vector, such as
$\left| \psi \right\rangle$. The inner product between vectors
$\left| \psi\right\rangle$ and $\left| \phi \right\rangle$ is denoted
$\left\langle\phi \big| \psi\right\rangle$. For a quantum mechanical
system for which we want to calculate the time evolution, the vector
describing the system becomes itself time-dependent:
$\left| \psi(t)\right\rangle$.

The equation determining the time evolution of a known state vector
$\left| \psi(0)\right\rangle$ at $t=0$ is known as the *time-dependent
Schrödinger equation*: 
$$
{\rm i}\hslash \frac{\partial}{\partial t} \left| \psi(t) \right\rangle= \hat{H} \left| \psi(t)\right\rangle. \tag{1.1}\label{eq:TDSE11}
$$
Here, $\hat{H}$ is an *operator* acting on vectors in the Hilbert space.

$\hat{H}$ is *Hermitian*, which means it is equal to its Hermitian
conjugate. The Hermitian conjugate $\hat{A}^\dagger$ of an operator
$\hat{A}$ is defined as follows: $\hat{A}^\dagger$ must be such that for
any two vectors $\psi$ and $\phi$ it must hold that
$$\left\langle\phi \right| \hat{A} \left| \psi \right\rangle= \left( \left\langle\psi \right| \hat{A}^\dagger \left|\phi\right\rangle\right)^*,$$

We summarise:

!!! note "Hermitian operators"
    
    $$\hat{A}^\dagger = \hat{A} \text{ means that } \hat{A} \text{ is Hermitian.}$$
    In particular, the Hamiltonian $\hat{H}$ is Hermitian: $\hat{H}^\dagger = \hat{H}$.
    
    Hermitian operators have important properties:

    -   The eigenvalues $\lambda$ of a Hermitian operator are all real:
        $\lambda = \lambda^*$.

    -   Eigenvectors $\left| \phi_\lambda  \right\rangle$ and
        $\left| \phi_\mu \right\rangle$ belonging *different* eigenvalues
        $\lambda$ and $\mu$ are always mutually orthogonal:
        $$\left\langle\phi_\lambda \big| \phi_\mu \right\rangle= 0; \phantom{xxx} \lambda \neq \mu.$$

    -   *Degenerate* eigenvectors can be chosen orthogonal.

    -   All eigenvectors span the Hilbert space.

### Identity operator

The fact that the eigenvectors form a basis of the Hilbert space leads
to the often-used *resolution of the identity*, in which the unit
operator $\unicode{120793}$ is written as:
$$\unicode{120793} = \sum_j \left| \phi_\lambda \right\rangle\left\langle  \phi_\lambda  \right|$$
where the sum is over all the eigenvectors.

### Solutions to Schrödinger equation

The solution to the time-dependent Schrödinger equation \eqref{eq:TDSE11} is easy to find: it is just
$$\left| \psi(t) \right\rangle= e^{-{\rm i}\hat{H} t/\hslash} \left| \psi(0) \right\rangle.$$
This solution is verified by substituting it back into that equation.
However, working out this solution is very difficult, as it contains the
exponential of an operator. The easiest way to handle this exponent is
by diagonalising the operator. Suppose we have a complete set of
eigenvectors $\left| \phi_n \right\rangle$ and eigenvalues $E_n$ for
$\hat{H}$: $$\tag{1.2}\label{eq:StatSchrodCh2}
\hat{H} \left| \phi_n\right\rangle= E_n \left| \phi_n \right\rangle,$$
and that we know how to expand the initial state
$\left| \psi(0)\right\rangle$ into these eigenstates. Given that the
exponent of an operator can be written as the diagonal operator with the
exponents of the eigenvalues on its diagonal (I write this here for a
finite-dimensional matrix):
$$e^{-{\rm i}t \hat{H}/\hslash} = \left( \begin{array}{ccccc} e^{-{\rm i}E_1 t/\hslash} & 0 & \cdots & 0 & 0 \\
                                       0 & e^{-{\rm i}E_2 t/\hslash} & \cdots & 0 & 0 \\
                                       0 & 0 & \ddots & 0 & 0 \\
                                       0 & 0 & \cdots & 0 & e^{-{\rm i}E_N t/\hslash} \end{array}\right),$$
we obtain
$$\left|\psi(t)\right\rangle= \sum_{j=1}^N c_j e^{-{\rm i}E_j t/\hslash} \left| \phi_j\right\rangle,$$
where $$c_j = \left\langle\phi_j | \psi(0)\right\rangle.$$
Eq. \eqref{eq:StatSchrodCh2} is called the *stationary Schrödinger
equation*.

### Diagonalising a matrix

In case you have forgotten how to diagonalise a matrix, I recall that,
since for an eigenvector $\left|\phi\right\rangle$ of an operator
$\hat{A}$,
$$\hat{A} \left| \phi \right\rangle= \lambda \left| \phi \right\rangle,$$
$\left| \phi \right\rangle$ should be a non-zero vector for which
$$(\hat{A} - \lambda \unicode{120793}) \left|\phi \right\rangle= 0.$$ From
your first lecture on linear algebra, you should know that this can only
be true if the determinant of the matrix $\hat{A}-\lambda \unicode{120793}$
vanishes. This leads to an algebraic equation for $\lambda$. As an
example, consider $$\hat{H} = \left( \begin{array}{cc} 0 & 1 \\
                           1 & 0 \end{array}\right).$$ The determinant
condition is $$\left| \begin{array}{cc} -\lambda & 1 \\
                 1 & -\lambda \end{array}\right| = 0,$$ which leads to
$$\lambda^2 = 1,$$ so $\lambda = \pm 1$. This could be anticipated as
you may have recognised $\hat{H}$ as the Pauli matrix $\sigma_x$ and
know that the Pauli matrices all have eigenvalues $\pm 1$. Obviously,
the larger the matrix, the higher the order of the equation for
$\lambda$ and the more work it takes to find the eigenvalues.

Once you have the eigenvalue, you may find the corresponding eigenvector
by solving linear equations. Calling the eigenvector $(a,b)$ we have,for
the $+1$ eigenvalue, $$\begin{aligned}
b & = a \\
a & = b 
\end{aligned}$$ and the normalised eigenvector becomes $(1,1)/\sqrt{2}$.
Similarly we find $(1,-1)/\sqrt{2}$ for the eigenvector with eigenvalue
$-1$. These eigenvectors could also have been guessed if you let
yourself be guided by the symmetric structure of the matrix you are
diagonalising. Another helpful fact is that, for a Hermitian matrix
(operator), the eigenvectors belonging to different eigenvalues are
orthogonal, and that for equal eigenvalues (degeneracy), all
eigenvectors can always be chosen orthogonal. The matrix eigenvalue
problem can be solved analytically only if the matrix size is modest
(typically smaller than or equal to 3) or if the matrix has a simple
and/or very regular structure. In all other cases we use numerical
routines for solving the eigenvalue problem.

### Time evolution

We see that the time evolution of a wave function is determined by the
*time evolution operator* $\hat{U}(t) = e^{-{\rm i}t \hat{H}/\hslash}$.
From the hermiticity of $\hat{H}$, it is easy to see that $\hat{U}(t)$
satisfies
$$\hat{U}(t) \hat{U}^\dagger(t) = \hat{U}^\dagger(t) \hat{U}(t) = \unicode{120793},$$
where $\unicode{120793}$ is the unit operator. An operator satisfying this
equation is called *unitary*. We see that unitarity of the time
evolution operator directly follows from the hermiticity of the
Hamiltonian. Interestingly, this unitarity also guarantees that the norm
of the wave function is conserved. To see this, we use $$\begin{aligned}
\left| \psi(t) \right\rangle& = \hat{U}(t) \left| \psi(0) \right\rangle; \\
\left\langle\psi(t) \right| & =  \left\langle\psi(0) \right| \hat{U}^\dagger(t),
\end{aligned}$$ to evaluate what happens to the norm as time evolves:
$$\left\langle\psi(t) | \psi(t) \right\rangle = \left\langle\psi(0) \right| \hat{U}^\dagger(t) \hat{U}(t) \left| \psi(0) \right\rangle=
\left\langle\psi(0) | \psi(0) \right\rangle,$$ which shows that the norm
is indeed preserved.

### Example quantum systems with their Hamiltonians

The Schrödinger equation is quite a general equation and does not
specify the structure of the Hilbert space, nor the specific form of the
Hamiltonian. Physicists have guessed both in the first decades of the
twentieth century and good guesses have turned out to yield results for
physical measurements in excellent agreement with experiment. Here we
list a few.

-   Spinless point particle in one dimension. Hilbert space: class of
    square integrable functions ($L_2$) on the real axis. Hamiltonian:
    $$\hat{H} = - \frac{\hslash^2}{2m} \frac{d^2}{dx^2} + V(x).$$

-   Spinless point particle in three dimensions. Hilbert space: class of
    square integrable functions ($L_2$) in ${\mathbb R}_3$. Hamiltonian:
    $$\hat{H} = -\frac{\hslash^2}{2m} \nabla^2 + V({\bf r}).$$

-   Particles with spin 1/2, neglecting their motion. Hilbert space:
    two-dimensional vector space. Hamiltonian:
    $$\hat{H} = \frac{eB}{m} \sigma_z,$$ where $B$ is a magnetic field
    along the $z$-axis and $\sigma_z$ is the Pauli matrix
    $$\left( \begin{array}{cc} 1 & 0 \\
                     0 & -1 \end{array}
    \right).$$

It is easy to extend this list with numerous other cases.

### Probability and expectation values

For any physical quantity $A$, appropriate for the system at hand, there
exists a Hermitian operator $\hat{A}$ whose eigenvalues $\lambda_n$ are
the possible values of $A$ found in a measurement. These values occur
with probability $\left|\left\langle\phi_n| \psi\right\rangle\right|^2$,
where $\phi_n$ is the eigenvector corresponding to $\lambda_n$ and
$\left| \psi\right\rangle$ is the state of the system. The expectation
value of $A$ in a system in quantum state $\left| \psi \right\rangle$ is
given by $\left\langle\psi \right| \hat{A} \left| \psi \right\rangle$.

### Radial potential

We now concentrate on electrons in 3D, moving in the field of a radial
potential depending only on the distance $r$ to the origin:
$V\equiv V(r)$, $r=\left| {\bf r}\right|$. This is a special example of
a system exhibiting a symmetry. If there is a symmetry, there usually is
*degeneracy*, meaning that two or more eigenvalues of the Hamiltonian
(i.e., the energies) have the same values -- we shall return to this
point in chapter [3](./ch31.md). It turns out that if there is symmetry, there
is one or more operators that commute with the Hamiltonian. The
expectation values of these operators (which are assumed to have no
explicit time dependence) then remain unchanged in time as can easily be
checked from the time evolution:
$$\left\langle A \right\rangle_t = \left\langle\psi_0 \left| e^{{\rm i}t H/\hslash} A e^{-{\rm i}t H/\hslash } \right| \psi_0 \right\rangle.$$
Taking the time derivative of this expression yields the commutator
$[H,A]$ which vanishes by assumption. It is possible to find a set of
vectors that are eigenvectors of all independent operators which commute
with $H$. To be specific, states whose *energy* eigenvalues are
degenerate may have different eigenvalues for an operator $\hat{A}$
other than $\hat{H}$. In order to identify all the states uniquely, we
need in addition to $\hat{H}$ a set of operators $\hat{A}$, $\hat{B}$,
$\hat{C}$,...such that each (simultaneous) eigenvector of this set of
operators has a *unique* set of eigenvalues $E_n, a_j, b_k, c_l$, ....
The set of all independent operators which commutes with $H$, including
$H$ is called *observation maximum*:

!!! note "Observation maximum"

    *An observation maximum* is the set of all independent operators, including H, that commute amongst themselves and with H. The eigenvalues of all these operators label the simultaneous eigenvectors of all the operators of the observation maximum. They form a basis of the Hilbert space.

### Degeneracy

Degeneracy is related to symmetry which is present in the Hamiltonian
(this is formally substantiated by the quantum mechanical version of
Noether's theorem, which we shall not go into here). This relation is
the reason why the energies of a 3D system which is spherically
symmetric (that is, a system with a radial potential), are degenerate.
The operators commuting with the Hamiltonian for a spinless particle in
a radial potential are the angular momentum operators $L^2$ and $L_z$.
These have the eigenvalues $\hslash^2 l(l+1)$ and $\hslash m$
respectively, where $l$ is $0, 1, 2, \ldots$ and $m$ is an integer
running from $-l$ to $l$. The energy eigenvalues depend on $l$ and an
additional quantum number, $n$ -- they are written as $E_{nl}$. For each
$l$, $m$ runs from $-l$ to $l$ in integer steps. So there are $2l+1$
$m$-values for each $l$. As the energy eigenvalues do *not* depend on
$m$, they are (at least) $2l+1$-fold degenerate. If the particles have
spin-1/2, there are additional quantum numbers: $s$ which always takes
on the value $1/2$ (as we are dealing with spin-1/2 particles) and $m_s$
which takes the value $\pm 1 /2$. All in all, the states of an electron
in a radial potential are denoted $\left| n, l, m, s, m_s\right\rangle$.
The quantum number $s$ being always 1/2 for an electron, is often left
out. If we include the spin, each level with quantum number $l$ is
$2(2l+1)$-fold degenerate.
Figure [1.1](#Fig:RadSpec){reference-type="ref" reference="Fig:RadSpec"}
shows a schematic representation of a spectrum of a particle in a radial
potential.

<figure markdown>
![Radial potential spectrum](figures/radspect.svg){width="auto"}
  <figcaption> The spectrum of an electron in a radial potential, grouped according
to the $l$-value. In parenthesis, the degeneracy of the levels
(including the two-fold spin-degeneracy) is given. </figcaption>
</figure>

### Coulomb potential

The Coulomb potential is a special case: this potential has some hidden
symmetry which causes several of the $E_{nl}$ to coincide[^1]. Whereas
we normally label the energy eigenvalues for each $l$ by
$n=1, 2, \ldots$, this degeneracy allows us to label the states as in the
figure below.

<figure markdown>
![Hydrogen spectrum](figures/hydrspect.svg){width="auto"}
  <figcaption> The spectrum of an electron in a Coulomb potential, grouped according
to their $l$-value. The numbers along the vertical axis is the principal
quantum number. The degeneracies per $l$-value are the same as for
radial potential (see figure above). Adding the degeneracies for each principal
quantum number $n$ gives the degeneracies in parentheses on the right
hand side. </figcaption>
</figure>


The quantum number along the vertical axis is
called the *principal quantum number*. We see that for each principal
quantum number $n$, we have states with $l$-values between $0$ and
$n-1$. This is a special degeneracy of the hydrogen atom -- the
degeneracies of the energy levels can be found by adding the
degeneracies for each $l=0, \ldots, n-1$. These degeneracies correspond
to the so-called 'noble gas' atoms (for higher $n$, deviations from this
series occur due to effects not taken into account here).

[^1]: This symmetry is related to the four-dimensional rotational group
    $O(4)$.
