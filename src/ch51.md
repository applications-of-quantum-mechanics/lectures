# 5. Scattering in classical and in quantum mechanics {#Chap:Scat}

Scattering experiments are perhaps the most important tool for obtaining
detailed information on the structure of matter, in particular the
interaction between particles. Examples of scattering techniques include
neutron and X-ray scattering for liquids, atoms scattering from crystal
surfaces, elementary particle collisions in accelerators. In most of
these scattering experiments, a beam of incident particles hits a target
which also consists of many particles. The distribution of scattering
particles over the different directions is then measured, for different
energies of the incident particles. This distribution is the result of
many individual scattering events. Quantum mechanics enables us, in
principle, to evaluate for an individual event the probabilities for the
incident particles to be scattered off in different directions; and this
probability is identified with the measured distribution.

Suppose we have an idea of what the potential between the particles
involved in the scattering process might look like, for example from
quantum mechanical energy calculations (programs for this purpose will
be discussed in the next few chapters). We can then *parametrise* the
interaction potential, i.e. we write it as an analytic expression
involving a set of constants: the parameters. If we evaluate the
scattering probability as a function of the scattering angles for
different values of these parameters, and compare the results with
experimental scattering data, we can find those parameter values for
which the agreement between theory and experiment is optimal. Of course,
it would be nice if we could evaluate the scattering potential directly
from the scattering data (this is called the *inverse problem*), but
this is unfortunately very difficult (if not impossible) as many
different interaction potentials can have similar scattering properties
as we shall see below.

Many different motivations for obtaining accurate interaction potentials
can be given. One is that we might use the interaction potential to make
predictions about the behaviour of a system consisting of many
interacting particles, such as a dense gas or a liquid.

Scattering might be *elastic* or *inelastic*. In the former case the
energy is conserved, in the latter energy disappears. This means that
energy transfer takes place from the scattered particles to degrees of
freedom which are not included explicitly in the system (inclusion of
these degrees of freedom would cause the energy to be conserved). In
this chapter we shall consider elastic scattering.

## 5.1. Classical analysis of scattering

A well known problem in classical mechanics is that of the motion of two
bodies attracting each other by a gravitational force whose value decays
with increasing separation $r$ as $1/r^2$. This analysis is also correct
for opposite charges which feel an attractive force of the same form
(Coulomb's law). When the force is repulsive, the solution remains the
same -- we only have to change the sign of the parameter $A$ which
defines the interaction potential according to $V(r) = A/r$. One of the
key experiments in physics which led to the notion that atoms consist of
small but heavy kernels, surrounded by a cloud of light electrons, is
*Rutherford scattering*. In this experiment, a thin gold sheet was
bombarded with $\alpha$-particles (i.e. helium-4 nuclei) and the
scattering of the latter was analysed using detectors behind the gold
film. In this section, we shall first formulate some new quantities for
describing scattering processes and then calculate those quantities for
the case of Rutherford scattering.

Rutherford scattering is chosen as an example here -- scattering
problems can be studied more generally; see Griffiths, chapter 11,
section 11.1.1 for a nice description of classical scattering.

We consider scattering of particles incident on a so-called 'scattering
centre', which may be another particle. The scattering centre is
supposed to be at rest. This might not always justified in a real
experiment, but in a standard approach in classical mechanics, the full
two-body problem is reduced to a one-body problem with with a reduced
mass, which is the present case (in
problem [5.1](./ch57.md) we will perform the same procedure for a
quantum two-body system). The incident particles interact with the
scattering centre located at ${\bf r}=
\pmb 0$ by the usual scalar two-point potential $V(r)$ which satisfies
the requirements of Newton's third law. Suppose we have a beam of
incident particles parallel to the $z$-axis. The beam has a homogeneous
density close to that axis, and we can define a *flux*, which is the
number of particles passing a unit area perpendicular to the beam, per
unit time. Usually, particles close to the $z$-axis will be scattered
more strongly than particles far from the $z$-axis, as the interaction
potential between the incident particles and scattering centre falls off
with their separation $r$. An experimentalist cannot analyse the
detailed orbits of the individual particles -- instead a detector is
placed at a large distance from the scattering centre and this detector
counts the number of particles arriving at each position. You may think
of this detector as a photographic plate which changes colour to an
extent related to the number of particles hitting it. The theorist wants
to predict what the experimentalist measures, starting from the
interaction potential $V(r)$ which governs the interaction process.

<figure markdown>
![Scat](figures/scat.svg){: width="500px" #Fig:Scat}
  <figcaption> FIGURE 5.1: Geometry of the scattering process. b is the impact parameter and $\varphi$ and $\vartheta$ are the angles of the orbit
of the outcoming particle </figcaption>
</figure>

In figure [5.1](#Fig:Scat), the geometry of the process is shown. In addition
a small cone, spanned by the spherical polar angles $d\vartheta$ and
$d\varphi$, is displayed. It is assumed here that the scattering takes
place in a small neighbourhood of the scattering centre, and for the
detector the orbits of the scattered particles all seem to be directed
radially outward from the scattering centre. The surface $dA$ of the
intersection of the cone with a sphere of radius $R$ around the
scattering centre is given by
$dA = R^2 \sin\vartheta d\vartheta d\varphi$. The quantity
$\sin\vartheta d\vartheta d\varphi$ is called *spatial angle* and is
usually denoted by $d\Omega$. This $d\Omega$ defines a cone like the one
shown in figure [5.1](#Fig:Scat). Now consider the number of particles which will
hit the detector within this small area per unit time. This number,
divided by the total incident flux (see above) is called the
*differential scattering cross section*, $d\sigma/
d\Omega$:
$$\frac{d\sigma(\Omega)}{d\Omega} = \frac{{\text{Number of particles leaving the scattering centre through the cone } d\Omega \text{ per
unit time}}}{{\text{Flux of incident beam } \times d\Omega}}.$$ The differential cross section
has the dimension of area (length$^2$).

First we realise ourselves that the problem is symmetric with respect to
rotations around the $z$-axis, so the differential scattering cross
section only depends on $\vartheta$. The only two relevant parameters of
the incoming particle then are its velocity and its distance $b$ from
the $z$-axis. This distance is called the *impact parameter* -- it is
also shown in figure [5.1](#Fig:Scat).

We first calculate the scattering angle $\vartheta$ as a function of the
impact parameter $b$. We perform this calculation for the example of
Rutherford scattering, for which we have the standard Kepler solution
which is now a hyperbola (see your classical mechanics lecture course).
The potential for the Kepler problem is $V(r) = -A/r$. The orbits are
given by specifying $r(t)$, $\vartheta(t)$. However, for the Kepler
problem it is more convenient to focus on the *shape* of the orbitals,
which is given as
$$r = \lambda \frac{1+\epsilon}{\epsilon \cos(\vartheta-C) - 1}$$ with
$$\tag{5.3}\label{Eq:KepHelp}
\epsilon = \sqrt{ 1 + \frac{2E \ell^2}{\mu A^2}};$$ this parameter is
called *eccentricity* -- for a hyperbola, we have $|\epsilon|>1$. Here,
$\ell$ is the angular momentum and $\mu$ the reduced mass. The
integration constant $C$ reappears in the cosine because we have not
chosen $\vartheta=0$ at the perihelion -- the closest approach occurs
when the particle crosses the dashed line in
figure [5.1](#Fig:Scat) which bisects the in- and outgoing particle
direction.

We know that for the incoming particles, for which $\vartheta=\pi$,
$r\rightarrow\infty$, we have $$\cos(\pi - C) = 1/\epsilon.$$ Because of
the fact that cosine is even $$\cos x = \cos (-x)$$ we can infer that
the other value of $\vartheta$ for which $r$ goes to infinity, and which
corresponds to the outgoing direction occurs when the argument of the
cosine is $C-\pi$, so that we find $$\vartheta_\infty - C = C - \pi,$$
or $\vartheta_\infty = 2C-\pi$. The subscript $\infty$ indicates that
this value corresponds to $t \rightarrow \infty$. From the last two
equations we find the following relation between the scattering angle
$\vartheta_\infty$ and $\epsilon$: $$\tag{5.6}\label{Eq:ThetaEps}
 \sin(\vartheta_\infty/2) = \cos(\pi/2-\vartheta/2) = \cos(\pi - C) = 1/\epsilon.$$

We want to know $\vartheta_\infty$ as a function of $b$ rather than
$\epsilon$ however. To this end we note that the angular momentum is
given as $$\ell = \mu v_{\text{inc}} b,$$ where 'inc' stands for
'incident', and the total energy as
$$E = \frac{\mu}{2} v^2_{\text{inc}},$$ so that the impact parameter can
be found as $$b = \frac{\ell}{\sqrt{2\mu E}}.$$ Using
Eq. \eqref{Eq:KepHelp} and the fact that
$\cot(x) = \sqrt{1-\sin^2(x)}/\sin(x)$, we can finally write
\eqref{Eq:ThetaEps} in the form: $$\tag{5.10}\label{Eq:BTheta}
\cot(\vartheta_\infty/2) = \sqrt{\epsilon^2 - 1} = \frac{2 E b}{|A|}.$$

From the relation between $b$ and $\vartheta_\infty$ we can find the
differential scattering cross section. The particles scattered with
angle between $\vartheta$ and $\vartheta+d\vartheta$, must have
approached the scattering centre with impact parameters between
particular boundaries $b$ and $b+db$. The number of particles flowing
per unit area through the ring segment with radius $b$ and width $db$ is
given as $j 2\pi b db$, where $j$ is the incident flux. We consider a
segment $d\varphi$ of this ring. Hence:
$$d\sigma(\Omega) = b(\vartheta) db d\varphi.$$ Relation
\eqref{Eq:BTheta} can be used to express the right hand side in
terms of $\vartheta_\infty$:
$$d\sigma(\Omega) = \left( \frac{A}{2E} \right)^2 \cot(\vartheta/2) \;
d\cot(\vartheta/2)\; d\varphi = \left( \frac{A}{2E} \right)^2 
\cot(\vartheta/2)
\frac{d\cot(\vartheta/2)}{d\vartheta}\frac{d\vartheta}{d\cos\vartheta} 
d \cos\vartheta d\varphi.$$ This can be worked out straightforwardly to
yield: $$\frac{d\sigma(\Omega)}{d\Omega} = \left( \frac{A}{4E} \right)^2
\frac{1}{\sin^4\vartheta/2}.$$ This is known as the famous *Rutherford
formula*.