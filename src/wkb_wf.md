---
title: WKB wave function
---

# 4.2. The WKB wave function

!!! pull "Expected prerequisites"

    Before the start of this lecture, you should be able to:

    1. Write down the Schrödinger equation of a particle in a one-dimensional potential.
    2. Solve first-order ordinary differential equations.

!!! goals "Learning goals"

    After this lecture you will be able to:

    1. Derive the WKB wave function for the propagating and evanescent regimes from an intuitive and formal approach.
    2. Write down the general solution of Schrödinger equation of a particle in a one-dimensional potential in WKB approximation.
    3. Explain and discuss the limits of the assumptions used to derive the WKB wave function.


## 4.2.1. Premise

In the chapter on the WKB approximation, we will focus on a specific problem: namely solving the quantum problem of one particle in a one-dimensional potential $V(x)$, where $x$ is the spatial coordinate in 1D. The stationary Schrödinger equation for the wave function $\psi(x)$ reads in this case

$$
-\frac{\hbar^2}{2m} \frac{d^2}{dx^2} \psi(x) + V(x) \psi(x) = E \psi(x)\,. \tag{4.1}\label{eq:schrodinger}
$$

Our goal now will be to derive an approximate result for the wave function $\psi(x)$ if the potential $V(x)$ is smooth (where smooth will still have to be properly defined).

In the previous section, we learned that smooth potentials prevent backscattering. Using this intuition, we will first derive an approximation for the wave function making use of some heuristic [^1]  arguments. Afterwards, we will justify this rather intuitive derivation by showing that we can get the same result using a formal approach, too.

The approximation for the wave function that we are deriving here is typically referred to as Wentzel-Kramer-Brillouin (WKB) approximation, named after the physicists Gregor Wentzel, Hendrik Anthony Kramers und Léon Brillouin.

## 4.2.2. Intuitive derivation

To derive the WKB approximation in an intuitive fashion we will use two main assumptions:

1.  There is no back reflection in a smooth potential.
2.  A smooth potential can be decomposed into piecewise constant pads. 

We can without restriction rewrite the wave function as
$$
\psi(x) = A(x)e^{i\varphi(x)}. \tag{4.2}\label{eq:ansatz}
$$
where $A(x)$ is the amplitude and $\varphi(x)$ the phase (with both being real).


```python inline
import common
common.configure_plotting()
import matplotlib.pyplot as plt
import numpy as np

def discretize(x, y, step):
    z = np.ones_like(y)
    n_points = len(x)
    n_new = int(max(x)/step)
    d = int(n_points/n_new)
    for i in range(0, n_new, 2):
        z[i*d:(i+2)*d] = y[(i+1)*d]
    return z

x = np.linspace(-1, 1, 200)*np.pi/2
x1 = np.linspace(0,3.01,200)
x2 = np.linspace(1,2,100)
y = np.sin(x)
y1 = np.heaviside(x1-1, 0 ) + np.heaviside(x1-2, 0 ) + np.heaviside(x1-3, 0 )
y2 = (5-np.sin(x2*5*np.pi))
y3 = (5-np.sin(x1*5*np.pi))
z1 = discretize(x, y, step=0.1)

fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(12, 4))
ax[0].plot(x, y, label='smooth', color='red', linewidth=3, zorder=100)
ax[0].plot(x, z1, label=r'$discrete$', color='black')
ax[0].set_ylabel(r'$V(x)$',fontsize=18)

ax[1].plot(x1, y1, label=r'V(x)', color='black')
ax[1].plot(x2, y2, label=r'$\psi(x)$', color='red')
ax[1].plot(x1, y3, color='red', linestyle='dotted', linewidth=0.5)
ax[1].set_ylim(-0.1, 8.5);
ax[1].vlines(x=[1, 2], ymin=1, ymax=7, linestyle='dashed', color='red')
ax[1].text(0.7, 3.1, r'$\varphi_i$', fontsize=18, color='red')
ax[1].text(2.1, 3.1, r'$\varphi_{i+1}$', fontsize=18, color='red')
ax[1].text(1.4, 6.5, r'$\Delta\varphi$', fontsize=18, color='red')

for axes in ax:
    axes.legend(fontsize=12)
    axes.set_xlabel(r'$x$',fontsize=18)
    axes.set_xticks([]);
    axes.set_yticks([]);
fig.show()
```

First, let us focus on the phase $\varphi(x)$ shown in the right plot above. When  moving along a single potential step, the phase acquires a constant shift proportional to the wave vector and the travelled distance,
$$
\varphi(x_{i+1}) = \varphi(x_{i})+ \Delta \varphi.
$$ 
Here we make use of the fact that there is no back reflection, so that it is sufficient to consider the contribution of a single wave. Over a constant potential, the acquired phase is $\Delta \varphi = p(x_i) \Delta x / \hbar$, where we define the momentum at position $x$ as 
$$
p(x)=\sqrt{2m(E-V(x))} \tag{4.3}\label{eq:momentum}.
$$
The total phase of the wave function is then obtained by summing the contributions of all the steps, that is,
$$
\psi(x) \sim \exp\left(\pm \frac{i}{\hbar} \sum_{j=0}^N p(x_j) \Delta x\right) =\exp\left( \frac{\pm i}{\hbar} \int_{x_0}^x p(x') d x'\right).
$$
In the last equality, we take the limit $\Delta x \rightarrow 0$ to convert the summation to an integral, and thus find an expression for $\varphi(x)$.

Next, let us find the amplitude $A(x)$. Note that the solution, which we have been constructing so far, is a travelling wave - recall that we assumed that there is no back reflection! Such a travelling wave has an associated probability current
$$j(x) = \frac{\hbar}{2 m i} \left(\psi(x) \frac{d}{dx} \psi^*(x) - \psi^*(x) \frac{d}{dx} \psi(x)\right)\,. \tag{4.4}$$
Inserting the ansatz \eqref{eq:ansatz} for the wave function into the above expression for the probability current, we find
$$
j(x) = \frac{\hbar}{m} A^2(x) \frac{d \varphi}{dx} = A^2(x) \frac{p(x)}{m}\,.
$$
From quantum mechanics, we know that the probability current is conserved, i.e. $j(x) = j_0 = \text{const}$. We can thus solve for $A(x)$ to find
$$
A^2(x) = \frac{m j_0}{p(x)}\quad \Rightarrow \quad A(x) = \sqrt{\frac{m j_0}{p(x)}}.
$$
Typically, the proportionality constants $m$ and $j_0$ are left out and we write
$$
A(x) \sim \frac{1}{\sqrt{p(x)}}.
$$
At first glance this seems strange - now the amplitude doesn't even have the correct units! The reason for doing this is that, in most cases, it will be sufficient to derive expressions of the wave function up to a global, constant factor - as we will see in the explicit examples derived in the next sections. If you however need a properly normalized wave function in your application, remember to normalize it properly at the end.

Combining our results for $\varphi(x)$ and $A(x)$, we then arrive at the wave function in WKB approximation:
$$
\psi_{WKB}(x) \sim \frac{1}{\sqrt{p(x)}} \exp\left( \pm \frac{i}{\hbar} \int_{x_0}^x p(x') d x'\right).
$$

!!! note "Notes on the energy dependence and turning points"

    1. Observe that the energy enters the formula as a parameter, instead of being an outcome of calculations.
    2. When $V(x_0)=E$, the amplitude $A$ of $\psi$ diverges. This means that the WKB approximation is no longer valid, i.e. we do need to restrict ourselves to strictly $E>V(x)$ at these points $x_0$. Observe that these points are in turn a function of energy, i.e. $x_0 = x_0(E)$.

## 4.2.3. Formal derivation
We start again from the Schrödinger equation and rewrite it using the definition \eqref{eq:momentum} of the momentum $p(x)$:
$$
\begin{align}\label{eq:schrod}
-\frac{\hbar^2}{2m} \frac{d^2}{dx^2} & \psi(x) + V(x) \psi(x) = E\psi(x)\\
\Rightarrow  \quad & \psi''(x) + \frac{p^2(x)}{\hbar^2} \psi(x) = 0.
\end{align}
$$
We restrict $E>V(x)$, so $p(x)$ is real, and we again use the general wave function ansatz \eqref{eq:ansatz}, i.e. $\psi(x) = A(x) \exp(i\varphi(x))$, where $A$ and $\varphi$ are real. In what follows, we will omit the $x$-dependence in the notation and simply write $\psi=A e^{i\varphi}$.

The second spatial derivative of $\psi$ is
$$ 
\psi'' = A''e^{i\varphi}+2i A' \varphi' e^{i\varphi}- A (\varphi')^2 e^{i\varphi} + i A \varphi'' e^{i\varphi}. 
$$
Inserting this expression in the Schrödinger equation and dividing by $e^{i \varphi}$ gives
$$
A'' + 2i A' \varphi' - A ( \varphi' )^2  + i A \varphi''  + \frac{p^2}{\hbar^2} A = 0,
$$
Both the real and imaginary part of this equation need to be zero individually, and since both $A$ and $\varphi$ are real, we obtained two simpler equations:
$$
\begin{align}
A \varphi'' + 2 A' \varphi' &= 0 \tag{4.5}\label{eq:imagpart}\\
A'' - A ( \varphi' )^2 + \frac{p^2}{\hbar^2} A &= 0 \tag{4.6}\label{eq:realpart}.
\end{align}
$$
The solution to \eqref{eq:imagpart}can be obtained by multiplying the equation by $\varphi'$. That is,
$$
\begin{align}
\varphi'\left(A \varphi'' + 2 A' \varphi'\right) = (A^2 \varphi')' & = 0 \\
 \Rightarrow \quad A & = \frac{c}{\sqrt{\varphi'}}.
\end{align}
$$
To solve \eqref{eq:realpart}, let us assume that $A'' \ll A(\varphi')^2$, i.e. we drop $A''$. Then, we find that the equation can be solved as

$$
\begin{align}
-A (\varphi')^2 + \frac{p^2}{\hbar^2} A & = 0\\
\Rightarrow \quad (\varphi')^2 & = \frac{p^2}{\hbar^2}\\
\Rightarrow \quad \varphi' & = \pm \frac{p}{\hbar}\\
\Rightarrow \quad \varphi(x) & = \pm \frac{1}{\hbar}\int_{x_0}^x p('x) dx'. 
\end{align}
$$

Then, we can conclude that the WKB wave function is given as,
$$
\psi(x) = \frac{1}{\sqrt{p(x)}} \exp\left( \pm \frac{i}{\hbar} \int_{x_0}^x p(x') d x'\right).
$$
For the case $E < V(x)$, we use the same argument as before to find exponentially decaying and exponentially growing solutions.

!!! note "Smooth potential approximation"
    
    In the derivation above, we assumed that $A'' \ll A(\varphi')^2$. Let us find out what this approximation mean physically:

    1. We can expect that the amplitude $A$ changes over the length scale over which $V(x)$ changes. 
    2. If we define this length scale as $\Delta L$, we can estimate that $A' \sim \frac{1}{\Delta L}$ and $A'' \sim \frac{1}{\Delta L^2}$. 
    3. On the other hand, $A(\varphi')^2$ is dominated by the change in $\varphi$, which is given by the oscillatory nature of the wave function and characterized by the wavelength $\lambda$. 
    4. We can thus estimate $A (\varphi')^2 \sim \frac{1}{\lambda^2}$. With this, we find the condition
    $$
    \begin{align}
    A''  & \ll A(\varphi')^2\\
    \Rightarrow \quad \frac{1}{\Delta L^2} & \ll \frac{1}{\lambda^2}\\
    \Rightarrow \qquad \lambda & \ll \Delta L.
    \end{align}
    $$
    This indeed implies that the potential $V(x)$ should change slowly compared to the wave length $\lambda$.

## 4.2.4. Evanescent waves: $E < V(x)$

We assumed in our derivation that $E>V(x)$, but it is straightforwardly extended to $E < V(x)$ as well. In this case, the wave function does not accumulate a phase, but accumulates a decaying amplitude. On the formal level, when $E< V(x)$
it is useful to write 
$$
p(x)=\sqrt{2m(E-V(x))}=i\sqrt{2m(V(x)-E)}=i|p(x)|.
$$
where $|p(x)|$ is now a positive and real function. The WKB function then is given by
$$
\psi_{WKB}(x) = \frac{1}{\sqrt{|p(x)|}} \exp\left( \frac{\pm 1}{\hbar} \int_{x_0}^x |p(x')| d x'\right).
$$

## 4.2.5. Summary

!!! note "Take-home message"

    We derived the wave functions in WKB approximation under the assumption of a slowly changing potential - slowly compared to the wavelength. Since the Schrödinger equation \eqref{eq:schrodinger} is a second-order differential equation, the general solution will be a linear superposition of the two fundamental solutions ($\pm$) we found above:

    $$
    \begin{split}
    \psi(x)_{E > V(x)} &=  \frac{A}{\sqrt{p(x)}}e^{\frac{i}{\hbar} \int_{x_0}^{x} p(x') dx'} &+ \frac{B}{\sqrt{p(x)}}e^{-\frac{i}{\hbar} \int_{x_0}^{x} p(x') dx'},\\
    \psi(x)_{E < V(x)} &=  \frac{C}{\sqrt{|p(x)}|}e^{-\frac{1}{\hbar} \int_{x_0}^{x} |p(x')| dx'} &+ \frac{D}{\sqrt{|p(x)|}}e^{\frac{1}{\hbar} \int_{x_0}^{x} |p(x')|dx'}.
    \end{split}
    $$

    Pay attention to the sign in the exponents! Here, the exponents for $E > V(x)$ are complex and for $E < V(x)$ real, so if you are not reading these expressions carefully, it might seem as if coefficients were swapped!

[^1]: A heuristic is something providing aid in the direction of the solution of a problem but otherwise unjustified or incapable of justification. So heuristic arguments are used to show what we might later attempt to prove, or what we might expect to find in a computer run. They are, at best, educated guesses. Source: https://primes.utm.edu/glossary/page.php?sort=Heuristic 
Read more: https://en.wikipedia.org/wiki/Heuristic