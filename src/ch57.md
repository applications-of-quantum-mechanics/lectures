# 5.7. Problems

## 5.1. Two body problem

Show that for two identical
particles, 1 and 2, with coordinates ${\bf r}_1$ and ${\bf r}_2$,
the kinetic energy can be written as
$$T = -\frac{\hslash^2}{2m} \left( \nabla^2_1 + \nabla^2_2\right) = -\frac{\hslash^2}{2M}  \nabla^2_{\bf R}- \frac{\hslash^2}{2\mu}\nabla^2_{\bf r},$$
where $\nabla_{\bf r}$ denotes a gradient with respect to
${\bf R}= \frac{{\bf r}_1+{\bf r}_2}{2}$ and $\nabla_{\bf r}$ a
gradient with respect to ${\bf r}= {\bf r}_1-{\bf r}_2$. Finally,
$M=2m$ and $\mu = m/2$.

The Hamiltonian then becomes
$$H = \left[ -\frac{\hslash^2}{2M} \nabla^2_{\bf R}- \frac{\hslash^2}{2\mu} \nabla^2_{\bf r}+ V(r)\right] \psi({\bf r}, {\bf R}) = E\psi({\bf r}, {\bf R}).$$
Also show that $\psi({\bf r}, {\bf R})$ can be written as
$\phi({\bf r})\chi({\bf R})$. Find suitable eigen-equations for
$\phi$ and $\chi$ (this is separation of variables).

## 5.2.  
Consider the 1-D scattering problem illustrated in the figure below,
with an arbitrary localised potential (without any particular
spatial symmetry) in Region II, and $V(x)=0$ in Regions I and III.

<figure markdown>
![FigScat](figures/Figure_Scattering.svg){: width="500px" #Fig:FigScat}
<figcaption> </figcaption>
</figure>


In Regions I and III, the solutions to the time-independent
Schrodinger equation take on the form
$$\langle x |\psi\rangle=\left\{\begin{array}{ll}
Ae^{{\rm i}kx}+Be^{-{\rm i}kx} & \mbox{in Region I}\\
Ce^{{\rm i}kx}+De^{-{\rm i}kx} & \mbox{in Region III}
\end{array}
\right.,$$ where $k=\sqrt{2mE}/\hbar$. For scattering from left to
right, $\{A,B,C,D\}=\{1,S_{\mathrm{LL}},S_{\mathrm{RL}},0\}$. For
scattering from right to left,
$\{A,B,C,D\}=\{0,S_{\mathrm{LR}},S_{\mathrm{RR}},1\}$.

1.  Show that, if $\left\langle x | \psi\right\rangle$ is a solution
    to the stationary Schrödinger equation, so is its complex
    conjugate $\left\langle x | \psi\right\rangle^*$. This is a
    consequence of time reversal invariance.

2.  Calculate the flux for a wave function
    $\psi(x) = \exp({\rm i}k x)$. Show that particle number
    conservation implies that
    $$\left|A\right|^2 - \left|B\right|^2 = \left|C\right|^2 - \left|D\right|^2.$$

3.  Show
    $S_{\mathrm{LL}}S_{\mathrm{RL}}^*=-S_{\mathrm{RR}}^*S_{\mathrm{LR}}$
    (Hint: use the linearity of the Schrodinger equation).

4.  Using the linearity of the Schrödinger equation and the
    conservation law found in (b), show that
    $S_{\mathrm{RL}}$=$S_{\mathrm{LR}}$. Thus, the transmission
    amplitude through the potential is symmetric, even though the
    potential has no left-right symmetry.

5.  Show that the *scattering matrix*, defined by
    $$S=\left(\begin{array}{cc}
    S_{\mathrm{LL}} & S_{\mathrm{LR}} \\
    S_{\mathrm{RL}} & S_{\mathrm{RR}}
    \end{array}
    \right),$$ is unitary.

6.  Are (c) and (d) necessary or simply sufficient conditions for
    $S$ to be unitary?

## 5.3. Resonant scattering with a 1D square potential
In this problem, we consider scattering in 1 dimension for the
potential in the following picture:

<figure markdown>
![Resonance](figures/resonance.svg){: width="600px" #Fig:resonance}
<figcaption> </figcaption>
</figure>

The the height of the barriers is $V_b$, and the well depth is
$V_w$. The barrier width is $b$ and the width of the well $w$. We
consider the transmission of an incoming particle (an electron) at
some energy $E$. We take the Bohr radius $a_0$ as the unit of
length, and the Rydberg ($-13.6 e$V) as the unit of energy. In these
units, the Schrödinger equation reads
$$\left[ -\frac{d^2}{dx^2} + V(x) \right] \psi(x) = E \psi(x).$$

In each region where the potential is constant, the wave function is
a linear combination of two independent functions. At each boundary
between two regions of constant potential, the wave functions should
be matched in value and derivative. This matching leads to a linear
relation between the two coefficients on the left and those on the
right.

Consider a separation between a region with potential $V_\text{R}$
on the right and $V_\text{L}$ on the left. The separation is located
at the (horizontal) coordinate $r_s$.

1.  Give the scattering matrix which relates the expansion
    coefficients on the left of $r_s$ to those at the right (suppose
    those at the right are given). The matrix elements depend on
    $V_{\text{L}}$, $V_{\text{R}}$, $E$ and $r_s$. The energy can be
    smaller or larger than $V_{\text{L}}$, $V_{\text{R}}$.

2.  If we want to find the transmission amplitude, we take on the
    very right exclusively a right-moving wave, with amplitude 1. By
    acting with the appropriate scattering matrices, you can find
    the wave functions in the the other regions. In the leftmost
    region the wave function has an incoming and an outgoing
    component. Use a computer to calculate the coefficients of the
    two waves in this leftmost region as a function of the energy,
    ranging from a value just above $0$ to just below $V_b$ (take
    about 200 values for the energy).\
    Parameter suggestion: $w = 7$, $V_b = 8.0$, $b=0.6$ and
    $V_w = -2.0$ (all in our 'natural' units).

3.  In the leftmost region, there is an incoming and a reflected
    wave. The transmission is defined as the amplitude of the
    outgoing wave in the rightmost region, provided that the
    amplitude of the incoming wave on the left is 1. Plot the
    transmission amplitude as a function of the energy.

4.  If your program works correctly, you find several peaks. Give an
    explanation for those peaks.

5.  Now imagine a potential as in the picture, but now with
    $b=\infty$. In that case, the problem is that of the square
    well. You can find the bound states of the well graphically by
    intersecting $\tan(kx)$ with the line $q$, where $k$ is the wave
    vector inside the well, and $q$ is the decay constant outside
    the well (see Griffiths, section 2.6). Find a few low-energy
    eigenstates Compare your results with those found for the
    scattering problem.

6.  Investigate the behaviour peak widths when you change $b$. Also
    investigate variations of the parameters $w$, $V_w$ and explain
    what you see

## 5.4.  
Obtain the total cross-section for the potential given by
$$V(r) = \begin{cases} -V_0 & {\text{ for }} r<a; \\
                        0 & {\text{ for }} r>a.
        \end{cases}$$ Explain the result you get when taking the low
energy limit?

## 5.5.
Consider scattering off a spherical cage (such as a bucky ball),
which is described by a $\delta$-function $$V(r) = g \delta (r-a).$$
Find the scattering amplitude.

## 5.6.
In time-dependent perturbation theory, we can calculate the
probability to move from some initial state to some final state as a
result of a perturbation which was turned on for a finite time.
Specifically, we write the Hamiltonian in the form
$$H = H_0 + H'(t),$$ where $H_0$ is the unperturbed Hamiltonian with
eigenstates $\phi_n$ and eigenenergies $E_n$.

The time-dependent solutions of $H_0$ have the form
$$\left| \psi_0(t) \right\rangle= \sum_n c_n e^{-{\rm i}E_n t/\hslash} \left| \phi_n \right\rangle,$$
where the $c_n$ are time-independent expansion coefficients.

For the full Hamiltonian, we write the time-dependent solutions in
the form
$$\left| \psi(t) \right\rangle= \sum_n c'_n(t) e^{-{\rm i}E_n t/\hslash} \left| \phi_n \right\rangle.$$
Note that the expansion coefficients $c'_n(t)$ are now
time-dependent. We have solved the time-dependent problem if we know
the explicit time-dependence of these coefficients.

1.  From now on, we will drop the prime from the coefficients $c$ of
    the full solution. Show that the $c_n$ satisfy the equation
    $$\tag{TDS}\label{Eq:TDS}
    {\rm i}\hslash \dot{c}_k = \sum_{n} \left\langle\phi_k | H' | \phi_n \right\rangle e^{{\rm i}\omega_{kn} t} c_n = \sum_{n} H'_{kn} 
    e^{{\rm i}\omega_{kn} t} c_n,$$ where
    $\omega_{kn} = (E_k-E_n)/\hslash$.

2.  Now suppose that we start off at $t\rightarrow - \infty$ with a
    state $\left| \phi_l \right\rangle$ for a single $n$. We want to
    know the probability to end up in some other state
    $\left| \phi_m \right\rangle$ with $m\neq l$. Show that the
    coefficient can be solved by requiring that $c_n$ on the right
    hand side of \eqref{Eq:TDS} can be replaced by $\delta_{nl}$. Which
    condition on $H'$ would justify this approach? Show that now
    $$c_m = \frac{1}{{\rm i}\hslash} \int_{-\infty}^t H'_{ml}(t') e^{{\rm i}\omega_{ml}t'} dt'.$$

3.  Now suppose that the Hamiltonian $H'$ is switched on at $-T$ and
    switched off again at $T$ where $T\gg \hslash \omega_{ml}$, and
    that it can be assumed to be constant in between these two
    times. Show that then
    $$c_m = \frac{2}{{\rm i}\hslash} H'_{ml} \frac{\sin(\omega_{ml} T)}{\omega_{ml}}.$$
    From the fact that
    $$\lim_{x\rightarrow\infty} \sin^2(a x)/(a^2 x) = \frac{\pi}{2} \delta(a),$$
    derive that the rate at which the probability to find the
    particle in state $m$ after $T$ increases, is given by
    $$\frac{2\pi}{\hslash^2} |H'_{ml}|^2 \delta(\omega_{ml}).$$

4.  Now consider a scattering problem, where the unperturbed states
    are given by plane waves with wave vector ${\bf k}$, which are
    as usual denoted by $\left| {\bf k}\right\rangle$. The
    transition rate from an initial state
    $\left| {\bf k}_{\text{I}}\right\rangle$ to *any* final state
    $\left| {\bf k}_{\text{F}}\right\rangle$ is then given by
    $$
    R = \int \frac{2\pi}{\hslash} \left| \left\langle {\bf k}_{\text{F}} \right| H' \left| {\bf k}_{\text{I}} \right\rangle \right|^2 \delta \left(E_{\text{I}} - E_{\text{F}}\right) d^3k_{\text{F}}.
    $$
    Show from this that the
    differential cross section for scattering is given by
    $$\frac{d\sigma}{d\Omega} = \frac{2\pi m^2}{\hslash^4} \left| \left\langle{\bf k}_{\text{F}}\right| H' \left| {\bf k}_{\text{I}}\right\rangle\right|^2,$$
    i.e., we have recovered the first Born approximation. In the
    expressions, we assume the normalisation
    $$\left\langle{\bf k}| {\bf r}\right\rangle= \frac{e^{-{\rm i}{\bf k}\cdot {\bf r}}}{(2\pi)^{3/2}}.$$

## 5.7.
In this problem we consider a *Josephson junction*: two
superconducting regions separated by a thin insulating layer. In
each superconduting island, Cooper pairs of charge $2e$ and mass
$2m$ are all condensed into one and the same ground state wave
function: $\psi_{\text{L}}$ for the left island and
$\psi_{\text{R}}$ for the right island. The wave functions can in
this problem be assumed to have the form
$$\psi_{\text{L}} ({\bf r}) = \rho e^{{\rm i}\phi_{\text{L}}(t)} \phantom{xx} \text{and} \phantom{xx} 
\psi_{\text{R}} ({\bf r}) = \rho e^{{\rm i}\phi_{\text{R}}(t)}.$$ In
this problem, we neglect the finite thickness of the layer.

1.  Assuming that the ground state energy in the left island is $E$,
    give the time-dependence of $\phi_{\text{L}}$.

2.  A constant voltage is applied accros the junction: on the right,
    the voltage is $V_{\text{R}}$; on the left, this is
    $V_{\text{L}}$. These voltages are constant in time.

    Argue that this leads to an increase of the phase difference
    $\delta = \phi_{\text{R}} - \phi_{\text{L}}$, which increases in
    time according to ($V=V_{\text{R}} - V_{\text{L}}$):
    $$\delta(t) = \frac{2e}{\hslash} \left(V_{\text{R}} - V_{\text{L}}\right)t = \frac{2e}{\hslash} Vt.$$

3.  In the chapter on scattering, you have seen that the current
    (which is the flux times the area) in a one-dimensional system
    is given as
    $$\frac{\hslash}{4{\rm i}m} \left( \psi^* \frac{d\psi}{dx} - \psi \frac{d \psi^*}{dx}\right)$$
    where the factor $4$ in the denominator deviates from the usual
    2 because of the Cooper pairs having a mass $2m$. In our case,
    where there is a step in the wave function on the left and right
    hand side, we approximate the gradient by
    $$\frac{d\psi}{dx} = \psi_{\text{R}} - \psi_{\text{L}}.$$
    Furthermore, we take for $\psi$ at the junction the average of
    $\psi_{\text{R}}$ and $\psi_{\text{L}}$.

    Show that these approximations lead to a current proportional to
    the sine of the phase difference $\delta$:
    $$I = I_c \sin\delta(t),$$ where $I_c$ is some constant (it is
    the critical current) which you do not need to calculate.

## 5.8.
Calculate the differential cross section $d\sigma/d\Omega$ in the
first Born approximation for a potential of the form
$$V(r) = -V_0 e^{-r/R}.$$ *Hint:*
$\int_0^\infty e^{-ar} r dr = 1/a^2.$ Note that $a$ may be complex
in this expression!

## 5.9.  *Scattering off a charge distribution*
We consider scattering of particles with charge $e$ off a charge
distribution $\rho({\bf r})$ located near the origin. The
electrostatic potential felt by the incoming particles is given as
the solution to the Poisson equation (in SI units):
$$-\nabla^2 \varphi({\bf r}) = \rho({\bf r})/\epsilon_0.$$

1.  Show that in Fourier space, the Poisson equation takes the form
    $$k^2 \tilde{\varphi}({\bf k}) = \tilde{\rho}({\bf k})/\epsilon_0,$$
    where $\tilde{\varphi}$ and $\tilde{\rho}$ denote the Fourier
    transforms of $\varphi({\bf r})$ and $\rho({\bf r})$.

    The potential energy is given by $e\varphi({\bf r})$.

2.  Show that the scattering amplitude is given by
    $$f(\theta,\phi) = - \frac{me}{2\pi\hslash^2} \frac{\tilde{\rho}({\bf q})}{\epsilon_0 q^2}.$$

## 5.10. 
In the Born approximation, the forward-scattering amplitude (the
scattering angle $\vartheta = 0$) is real, yielding
$\sigma_{\text{tot}} = 0$, and therefore it appears to be in
contradiction with the optical theorem.

1.  Resolve this contradiction.

2.  Show that the second-order Born approximation yields a
    perturbative correction to the scattering amplitude which, for
    $\vartheta = 0$, is given by
    $$f^{(2)}(\vartheta=0) = \left( \frac{m}{2\pi\hslash^2}\right)^2e^{{\rm i}{\bf k}\cdot({\bf r}-{\bf r}')} \int V({\bf r}) \frac{e^{{\rm i}k|{\bf r}-{\bf r}'|}}{|{\bf r}-{\bf r}'|} V({\bf r}')\; d^3r \; d^3r'.$$

3.  Show that the optical theorem is satisfied to second order in
    the potential $V$.


