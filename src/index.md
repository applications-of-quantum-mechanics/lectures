# Applications of Quantum Mechanics

This is an interactive version of the lecture notes for the course AP3303 at TU Delft.

These notes have grown out of notes written for the course *Advanced Quantum Mechanics*.
which is part of the Master Programme Applied Physics at Delft University of Technology. 
They are the result of teaching activities in the field of quantum mechanics over a range of years and at different levels. 

The topics covered in these notes mostly concern techniques for addressing particular systems and problems in quantum mechanics.
We start however with a general overview of quantum mechanics and address its formal aspects. We discuss 
powerful approximative methods: variational calculus and WKB, before moving into
time-dependent systems and discussing the adiabatic approximation and Berry phases. We also address scattering in quantum mechanics.

Elaborate exercises are an integral part of the process of learning quantum mechanics, and the material can simply not be 
mastered without going through these exercises. 

Quantum mechanics at this level has two faces; it is hard material and requires a lot of effort. But it is also great fun!
It might happen that you will spending long hours trying to find the solution to the problems. Try not to give up and discuss your progress with your peers or teachers; the satisfaction of completing a solution will let you forget the frustration that you've gone through. 

!!! tip "Learning goals"

    After following this course you will be able to:

    1. Discuss the topics of the course description by clarifying assumptions and limitations of the theoretical models.
    2. Analyse applications of quantum mechanics through calculations.
    3. Evaluate relevant scientific literature by understanding and reproducing the conclusions
    4. Theorize a research problem by applying the theory of quantum mechanics.

!!! live-code "Jupyter notebook mode" 
    Look for :fontawesome-solid-wand-magic-sparkles: icon in the title of a lecture note and click it to turn lecture notes into an interactive Jupyter notebook!
    

!!! abstract "Learning materials"
    With these notes, our aim is to provide learning materials which are:

    - self-contained
    - interactive with live code
    - easy to modify and remix, so we provide the [full source](https://gitlab.kwant-project.org/applications-of-quantum-mechanics/lectures), including the [code](https://gitlab.kwant-project.org/applications-of-quantum-mechanics/lectures/-/tree/master/src/code)
    - open for reuse: see the [license](https://gitlab.kwant-project.org/applications-of-quantum-mechanics/lectures/-/blob/master/LICENSE.md).

!!! question "Contributions"
    Whether you are a student taking this course, or an instructor reusing the materials, we welcome all contributions, so check out the [course repository](https://gitlab.kwant-project.org/applications-of-quantum-mechanics/lectures), especially do [let us know](https://gitlab.kwant-project.org/applications-of-quantum-mechanics/lectures/issues/new?issuable_template=typo) if you see a typo!
