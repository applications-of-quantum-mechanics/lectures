# Guided exercise

In the first 30 minutes of the lecture I ask you to look at the "guided exercise below".

I have recorded solutions to all of the subproblems. The total length of the videos is
21 minutes and 16 seconds. If you have time before the lecture, you can try to solve the 
problems explicitly before watching the videos. If you only have time during the lecture,
then please briefly think for each problem how you would solve it (without doing the
calculation explicitly), and test your expectations with respect to the solutions in the videos!

## Expanding quantum well

Consider a quantum well with hard wall boundaries with a width that
changes in time, $W=W(t)$. One of the walls moves with constant
velocity $v$ such that the well width changes from $W_1 = W(0)$ to
$W_2 = W(T)$ with $W_2>W_1$.

1.  Write down the instantaneous eigenstates $\psi_n(t)$ and eigenenergies $E_n(t)$ 
    of the problem. Don't forget wave function normalization!
    Sketch the instantaneous eigenenergies as a function of $W(t)$.

    **Solution**
    <iframe width="100%" height=315 src="https://www.youtube-nocookie.com/embed/KnEcyJhrjnA?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

2.  Compute the total dynamical phase of the process, i.e. $\theta_n(T) = -\frac{1}{\hbar} \int_0^T E_n(t) dt$.
    Does this phase depend on the speed with which the wall moves?

    **Solution**
     <iframe width="100%" height=315 src="https://www.youtube-nocookie.com/embed/1xV6_l_yLFw?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

3. Compute the geometrical phase of the process, i.e. $\gamma_n(T) = i \int_0^T \langle \psi_n(t)|\frac{d}{dt} \psi_n(t)\rangle dt$.
    
    **Hint**
     <iframe width="100%" height=315 src="https://www.youtube-nocookie.com/embed/2Wa_uWfSAE4?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

     **Solution**
      <iframe width="100%" height=315 src="https://www.youtube-nocookie.com/embed/ZwCD0azjsWU?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
